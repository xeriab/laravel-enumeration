<?php /** @noinspection PhpUnnecessaryCurlyVarSyntaxInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * MakeEnumCommand
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Commands;

use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputOption;
use function file_exists;

/**
 * MakeEnumCommand Class.
 *
 * @package Exen\Laravel\Enumeration\Console
 */
class MakeEnumCommand extends GeneratorCommand
{
    /**
     * The console command name.
     *
     * @var string $name
     */
    protected $name = 'make:enum';

    /**
     * The console command description.
     *
     * @var string $description
     */
    protected $description = 'Create a new enum class';

    /**
     * The type of class being generated.
     *
     * @var string $type
     */
    protected $type = 'Enum';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub(): string
    {
        return $this->option('flagged')
            ? $this->resolveStubPath('/Stubs/FlaggedEnum.stub')
            : $this->resolveStubPath('/Stubs/Enum.stub');
    }

    /**
     * Resolve the fully-qualified path to the stub.
     *
     * @param string $stub
     *
     * @return string
     */
    protected function resolveStubPath(string $stub): string
    {
        return file_exists($customPath = $this->laravel->basePath(trim($stub, '/')))
            ? $customPath
            : __DIR__ . $stub;
    }

    /**
     * Get the default namespace for the class.
     *
     * @param string $rootNamespace
     *
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace): string
    {
        return "{$rootNamespace}\Enums";
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions(): array
    {
        return [
            ['flagged', 'f', InputOption::VALUE_NONE, 'Generate a flagged enum'],
        ];
    }
}

# vim: set ts=4 sw=4 tw=80 noet :
