<?php /** @noinspection PhpUnnecessaryCurlyVarSyntaxInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * AbstractAnnotationCommand
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Commands;

use Composer\Autoload\ClassMapGenerator;
use Illuminate\Console\Command;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Filesystem\Filesystem;
use InvalidArgumentException;
use Laminas\Code\Generator\DocBlockGenerator;
use Laminas\Code\Reflection\DocBlockReflection;
use ReflectionClass;
use ReflectionException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use function array_keys;
use function array_map;
use function is_subclass_of;
use function preg_quote;
use function preg_replace;
use function sprintf;
use function strlen;
use function strpos;
use function substr_replace;

/**
 * AbstractAnnotationCommand Class.
 *
 * @abstract
 *
 * @package Exen\Laravel\Enumeration\Console
 */
abstract class AbstractAnnotationCommand extends Command
{
    public const PARENT_CLASS = '';

    /**
     * Filesystem.
     *
     * @var Filesystem $filesystem
     */
    protected Filesystem $filesystem;

    /**
     * @param Filesystem $filesystem
     *
     * @return void
     */
    public function __construct(Filesystem $filesystem)
    {
        parent::__construct();

        $this->filesystem = $filesystem;
    }

    /**
     * Handle the command call.
     *
     * @return integer
     * @throws ReflectionException
     */
    public function handle(): int
    {
        if ($this->argument('class')) {
            $this->annotateClass($this->argument('class'));

            return 0;
        }

        $this->annotateFolder();

        return 0;
    }

    /**
     * Annotate a specific class by name
     *
     * @param string $className
     *
     * @return void
     *
     * @throws InvalidArgumentException
     * @throws ReflectionException
     */
    protected function annotateClass(string $className): void
    {
        if (!is_subclass_of($className, static::PARENT_CLASS)) {
            throw new InvalidArgumentException(
                sprintf('The given class must be an instance of %s: %s', static::PARENT_CLASS, $className)
            );
        }

        $reflection = new ReflectionClass($className);
        $this->annotate($reflection);
    }

    abstract protected function annotate(ReflectionClass $reflectionClass);

    /**
     * Annotate classes in a given folder
     *
     * @return void
     * @throws ReflectionException
     */
    protected function annotateFolder(): void
    {
        $classMap = ClassMapGenerator::createMap($this->searchDirectory());

        $classes = array_map(function ($class) {
            return new ReflectionClass($class);
        }, array_keys($classMap));

        foreach ($classes as $reflection) {
            if ($reflection->isSubclassOf(static::PARENT_CLASS)) {
                $this->annotate($reflection);
            }
        }
    }

    abstract protected function searchDirectory(): string;

    protected function getArguments(): array
    {
        return [
            ['class', InputArgument::OPTIONAL, 'The class name to generate annotations for'],
        ];
    }

    protected function getOptions(): array
    {
        return [
            ['folder', null, InputOption::VALUE_OPTIONAL, 'The folder to scan for classes to annotate'],
        ];
    }

    /**
     * Write new DocBlock to the class
     *
     * @param ReflectionClass $reflectionClass
     * @param DocBlockGenerator $docBlock
     *
     * @return void
     * @throws FileNotFoundException
     */
    protected function updateClassDocblock(
        ReflectionClass   $reflectionClass,
        DocBlockGenerator $docBlock
    ): void
    {
        $shortName = $reflectionClass->getShortName();
        $fileName = $reflectionClass->getFileName();
        $contents = $this->filesystem->get($fileName);

        $classDeclaration = "class {$shortName}";

        if ($reflectionClass->isFinal()) {
            $classDeclaration = "final {$classDeclaration}";
        } elseif ($reflectionClass->isAbstract()) {
            $classDeclaration = "abstract {$classDeclaration}";
        }

        // Remove existing docblock
        $contents = preg_replace(
            sprintf(
                '#([\n]?\/\*(?:[^*]|\n|(?:\*(?:[^\/]|\n)))*\*\/)?[\n]?%s#ms',
                preg_quote($classDeclaration)
            ),
            "\n" . $classDeclaration,
            $contents
        );

        $classDeclarationOffset = strpos($contents, $classDeclaration);

        $block = sprintf("%s%s", $docBlock->generate(), $classDeclaration);
        $block = preg_replace('/\s\*\s@package/ui', " *\n * @package", $block);

        // Make sure we don't replace too much
        $contents = substr_replace(
            $contents,
            $block,
            $classDeclarationOffset,
            strlen($classDeclaration)
        );

        $this->filesystem->put($fileName, $contents);

        $this->info("Wrote new phpDocBlock to {$fileName}.");
    }

    protected function getDocBlock(ReflectionClass $reflectionClass): DocBlockGenerator
    {
        $docBlock = DocBlockGenerator::fromArray([])->setWordWrap(false);

        $originalDocBlock = null;

        if ($reflectionClass->getDocComment()) {
            $originalDocBlock = DocBlockGenerator::fromReflection(
                new DocBlockReflection(ltrim($reflectionClass->getDocComment()))
            );

            if ($originalDocBlock->getShortDescription()) {
                $docBlock->setShortDescription($originalDocBlock->getShortDescription());
            }

            if ($originalDocBlock->getLongDescription()) {
                $docBlock->setLongDescription($originalDocBlock->getLongDescription());
            }
        }

        $docBlock->setTags(
            $this->getDocblockTags(
                $originalDocBlock ? $originalDocBlock->getTags() : [],
                $reflectionClass,
                true
            )
        );

        return $docBlock;
    }

    /**
     * @param array $originalTags
     * @param ReflectionClass $reflectionClass
     * @param boolean $ignoreDefault
     *
     * @return array
     */
    abstract protected function getDocblockTags(
        array           $originalTags,
        ReflectionClass $reflectionClass,
        bool            $ignoreDefault
    ): array;
}
