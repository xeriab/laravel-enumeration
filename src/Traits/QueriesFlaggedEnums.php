<?php /** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * QueriesFlaggedEnums
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Traits;

use Illuminate\Database\Eloquent\Builder;
use function array_sum;

/**
 * QueriesFlaggedEnums Class.
 *
 * @package Exen\Laravel\Enumeration\Traits
 */
trait QueriesFlaggedEnums
{
    /**
     * @param Builder $query
     * @param string $column
     * @param int $flag
     *
     * @return Builder
     */
    public function scopeHasFlag(Builder $query, string $column, int $flag): Builder
    {
        return $query->whereRaw("$column & ? > 0", [$flag]);
    }

    /**
     * @param Builder $query
     * @param string $column
     * @param int $flag
     *
     * @return Builder
     */
    public function scopeNotHasFlag(Builder $query, string $column, int $flag): Builder
    {
        return $query->whereRaw("not $column & ? > 0", [$flag]);
    }

    /**
     * @param Builder $query
     * @param string $column
     * @param int[] $flags
     *
     * @return Builder
     */
    public function scopeHasAllFlags(Builder $query, string $column, array $flags): Builder
    {
        $mask = array_sum($flags);

        return $query->whereRaw("$column & ? = ?", [$mask, $mask]);
    }

    /**
     * @param Builder $query
     * @param string $column
     * @param int[] $flags
     *
     * @return Builder
     */
    public function scopeHasAnyFlags(Builder $query, string $column, array $flags): Builder
    {
        $mask = array_sum($flags);

        return $query->whereRaw("$column & ? > 0", [$mask]);
    }
}

# vim: set ts=4 sw=4 tw=80 noet :
