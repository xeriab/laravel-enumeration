<?php /** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * EnumServiceProvider
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration;

/**
 * EnumServiceProvider Class.
 *
 * @package Exen\Laravel\Enumeration
 */
class EnumServiceProvider extends EnumerationServiceProvider
{
}

# vim: set ts=4 sw=4 tw=80 noet :
