<?php /** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * EnumType
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use function array_map;
use function implode;

/**
 * This type allows you to call ->change() on an enum column definition
 * when using migrations.
 */
class EnumType extends Type
{
    const ENUM = 'enum';

    /**
     * Gets the SQL declaration snippet for a field of this type.
     *
     * @param array $fieldDeclaration The field declaration.
     * @param AbstractPlatform $platform The currently used database platform.
     *
     * @return string
     * @noinspection PhpParameterNameChangedDuringInheritanceInspection
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        $values = implode(
            ',',
            array_map(static fn(string $value) => "'$value'", $fieldDeclaration['allowed'])
        );

        return "ENUM($values)";
    }

    /**
     * Gets the name of this type.
     *
     * @return string
     */
    public function getName(): string
    {
        return self::ENUM;
    }

    /**
     * @param AbstractPlatform $platform
     *
     * @return string[]
     */
    public function getMappedDatabaseTypes(AbstractPlatform $platform): array
    {
        return [
            self::ENUM,
        ];
    }
}

# vim: set ts=4 sw=4 tw=80 noet :
