<?php /** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * EnumerationServiceProvider
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration;

use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Types\Type;
use Exen\Laravel\Enumeration\Commands\EnumAnnotateCommand;
use Exen\Laravel\Enumeration\Commands\MakeEnumCommand;
use Exen\Laravel\Enumeration\Rules\Enum;
use Exen\Laravel\Enumeration\Rules\EnumKey;
use Exen\Laravel\Enumeration\Rules\EnumValue;
use Illuminate\Support\ServiceProvider;
use ReflectionException;
use function class_exists;
use function json_decode;
use function lang_path;
use function strtolower;

/**
 * EnumerationServiceProvider Class.
 *
 * @package Exen\Laravel\Enumeration
 */
class EnumerationServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     * @throws Exception
     * @throws ReflectionException
     */
    public function boot(): void
    {
        $this->bootCommands();
        $this->bootValidationTranslation();
        $this->bootValidators();
        $this->bootDoctrineType();
    }

    /**
     * Boot the custom commands
     *
     * @return void
     */
    private function bootCommands(): void
    {
        $this->publishes([
            __DIR__ . '/Commands/Stubs' => $this->app->basePath('stubs')
        ], 'stubs');

        if ($this->app->runningInConsole()) {
            $this->commands([
                EnumAnnotateCommand::class,
                MakeEnumCommand::class,
            ]);
        }
    }

    private function bootValidationTranslation()
    {
        $this->publishes([
            __DIR__ . '/../lang' => lang_path('vendor/exen-laravel-enumeration'),
        ], 'translations');

        $this->loadTranslationsFrom(__DIR__ . '/../lang/', 'exen-laravel-enumeration');
    }

    /**
     * Boot the custom validators
     *
     * @return void
     * @throws ReflectionException
     * @noinspection PhpUnusedParameterInspection
     */
    private function bootValidators(): void
    {
        $this->app['validator']->extend('enum_key', function ($attribute, $value, $parameters, $validator) {
            $enum = $parameters[0] ?? null;

            return (new EnumKey($enum))->passes($attribute, $value);
        }, __('exen-laravel-enumeration::messages.enum_key'));

        $this->app['validator']->extend('enum_value', function ($attribute, $value, $parameters, $validator) {
            $enum = $parameters[0] ?? null;

            $strict = $parameters[1] ?? null;

            if (!$strict) {
                return (new EnumValue($enum))->passes($attribute, $value);
            }

            $strict = !!json_decode(strtolower($strict));

            return (new EnumValue($enum, $strict))->passes($attribute, $value);
        }, __('exen-laravel-enumeration::messages.enum_value'));

        $this->app['validator']->extend('enum', function ($attribute, $value, $parameters, $validator) {
            $enum = $parameters[0] ?? null;

            return (new Enum($enum))->passes($attribute, $value);
        }, __('exen-laravel-enumeration::messages.enum'));
    }

    /**
     * Boot the Doctrine type.
     *
     * @return void
     * @throws Exception
     */
    private function bootDoctrineType(): void
    {
        // Not included by default in Laravel
        if (class_exists('Doctrine\DBAL\Types\Type')) {
            if (!Type::hasType(EnumType::ENUM)) {
                Type::addType(EnumType::ENUM, EnumType::class);
            }
        }
    }
}

# vim: set ts=4 sw=4 tw=80 noet :
