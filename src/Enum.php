<?php /** @noinspection PhpMultipleClassDeclarationsInspection */
/** @noinspection PhpUnusedAliasInspection */
/** @noinspection GrazieInspection */
/** @noinspection PhpUnusedLocalVariableInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * Enum
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration;

use ArrayIterator;
use Exception;
use Exen\Laravel\Enumeration\Attributes\Description;
use Exen\Laravel\Enumeration\Attributes\Label;
use Exen\Laravel\Enumeration\Casts\EnumCast;
use Exen\Laravel\Enumeration\Contracts\EnumContract;
use Exen\Laravel\Enumeration\Contracts\LocalizedEnum;
use Exen\Laravel\Enumeration\Exceptions\InvalidEnumKeyException;
use Exen\Laravel\Enumeration\Exceptions\InvalidEnumMemberException;
use Illuminate\Contracts\Database\Eloquent\Castable;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use Illuminate\Contracts\Database\Eloquent\CastsInboundAttributes;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Str;
use Illuminate\Support\Traits\Macroable;
use Iterator;
use JsonSerializable;
use ReflectionAttribute;
use ReflectionClass;
use ReflectionException;
use ReturnTypeWillChange;
use function __;
use function array_key_exists;
use function array_keys;
use function array_map;
use function array_rand;
use function array_search;
use function array_values;
use function class_implements;
use function collect;
use function constant;
use function count;
use function ctype_lower;
use function ctype_upper;
use function get_class;
use function implode;
use function in_array;
use function is_array;
use function is_bool;
use function is_null;
use function is_string;
use function is_subclass_of;
use function preg_replace;
use function property_exists;
use function sprintf;
use function str_replace;
use function str_starts_with;
use function strlen;
use function strtolower;
use function strtoupper;
use function substr;
use function trigger_error;
use function ucfirst;
use function ucwords;
use const E_USER_WARNING;

/**
 * Abstract Enum Class.
 *
 * Create an enum by implementing this class and adding class constants.
 *
 * @abstract
 *
 * @property mixed value
 * @property mixed key
 * @property integer|null ordinal
 * @property mixed|string label
 * @property mixed|string description
 *
 * @method mixed value()
 * @method mixed key()
 * @method integer|null ordinal()
 * @method string label()
 * @method mixed|string description()
 * @method string toString()
 *
 * @psalm-template T
 * @psalm-immutable
 * @psalm-consistent-constructor
 *
 * @package Exen\Laravel\Enumeration
 *
 * @phpstan-consistent-constructor
 */
abstract class Enum implements EnumContract, Castable, Arrayable, JsonSerializable
{
    use Macroable {
        __callStatic as macroCallStatic;
        __call as macroCall;
    }

    /**
     * Constant with default value for creating enum object.
     *
     * @var mixed|null Default
     */
    const Default = null;

    /**
     * @var boolean $unknownValuesFallbackToDefault
     */
    protected static bool $unknownValuesFallbackToDefault = false;

    /**
     * @var array $labels
     */
    protected static array $labels = [];

    /**
     * Caches reflections of enum subclasses.
     *
     * @var array<class-string<static>, ReflectionClass<static>> $reflectionCache
     */
    protected static array $reflectionCache = [];

    /**
     * @var array $cache
     */
    protected static array $cache = [];

    /**
     * The value of one of the enum members.
     *
     * @var mixed $value
     * @psalm-var T
     */
    public mixed $value;

    /**
     * The ordinal number of the enumerator.
     *
     * @var integer|null $ordinal
     */
    public ?int $ordinal = null;

    /**
     * The key of one of the enum members.
     *
     * @var mixed $key
     */
    public mixed $key;

    /**
     * The description of one of the enum members.
     *
     * @var mixed $description
     */
    public mixed $description;

    /**
     * The label of one of the enum members.
     *
     * @var mixed $label
     */
    public mixed $label;

    /**
     * Construct an Enum instance.
     *
     * @psalm-pure
     *
     * @param mixed|null $enumValue
     *
     * @psalm-param T $value
     *
     * @return void
     *
     * @throws ReflectionException
     * @throws InvalidEnumMemberException
     */
    public function __construct(mixed $enumValue = null)
    {
        self::bootClass();

        if (is_null($enumValue)) {
            $enumValue = static::Default;
        }

        if (!static::hasValue($enumValue)) {
            if (static::$unknownValuesFallbackToDefault) {
                $enumValue = static::Default;
            } else {
                throw new InvalidEnumMemberException($enumValue, $this);
            }
        }

        // Trick below is needed to make sure the value of original type gets set
        $this->value = static::values()[array_search($enumValue, static::values(), true)];
        // $this->value = $enumValue;
        $this->key = static::getKey($enumValue);
        $this->ordinal = static::getOrdinal($this->key);
        $this->description = static::getDescription($enumValue);
        $this->label = static::getLabel($enumValue);
    }

    /**
     * Initializes the constants array for the class if necessary.
     *
     * @param boolean $ignoreDefault
     *
     * @return void
     * @throws ReflectionException
     */
    protected static function bootClass(bool $ignoreDefault = true): void
    {
        if (!array_key_exists(static::class, static::$cache)) {
            $constants = self::getReflection()->getConstants();

            if ($ignoreDefault and array_key_exists('Default', $constants)) {
                unset($constants['Default']);
            }

            static::$cache[static::class] = $constants;

            if (method_exists(static::class, 'boot')) {
                /** @noinspection PhpUndefinedMethodInspection */
                static::boot();
            }
        }
    }

    /**
     * Get all the constants defined on the class.
     *
     * @param boolean $ignoreDefault
     *
     * @return array
     * @throws ReflectionException
     */
    protected static function getConstants(bool $ignoreDefault = true): array
    {
        self::bootClass($ignoreDefault);

        return static::$cache[static::class];
    }

    /**
     * Returns a reflection of the enum subclass.
     *
     * @return ReflectionClass<static>
     * @throws ReflectionException
     */
    protected static function getReflection(): ReflectionClass
    {
        $class = static::class;

        return static::$reflectionCache[$class] ??= new ReflectionClass($class);
    }

    /**
     * Check that the enum contains a specific value.
     *
     * @param mixed $value
     * @param boolean $strict (Optional, defaults to True)
     *
     * @return boolean
     * @throws ReflectionException
     */
    public static function hasValue(mixed $value, bool $strict = true): bool
    {
        $validValues = static::getValues();

        if ($strict) {
            return in_array($value, $validValues, true);
        }

        /** @noinspection SpellCheckingInspection */
        return in_array((string)$value, array_map('strval', $validValues), true);
    }

    /**
     * Get all or a custom set of the enum values.
     *
     * @param mixed ...$keys
     *
     * @return array
     * @throws ReflectionException
     */
    public static function getValues(mixed ...$keys): array
    {
        /** @var mixed $list */
        $list = null;

        if (count($keys) === 1) {
            $list = $keys[0];
        } elseif (count($keys) >= 1) {
            $list = $keys;
        }

        if ($list === null) {
            return array_values(static::getConstants());
        }

        return collect($list)
            ->map(static fn($key, $value) => static::getValue($key))
            ->toArray();
    }

    // /**
    //  * Gets Enum Ordinal.
    //  *
    //  * @return integer|null
    //  * @throws ReflectionException
    //  */
    // public function getOrdinal(): ?int
    // {
    //     return $this->ordinal;
    // }

    // /**
    //  * Gets Enum Key.
    //  *
    //  * @return mixed
    //  * @throws ReflectionException
    //  */
    // public function getKey(): mixed
    // {
    //     return $this->key;
    // }

    // /**
    //  * Gets Enum Value.
    //  *
    //  * @return mixed
    //  * @throws ReflectionException
    //  */
    // public function getValue(): mixed
    // {
    //     return $this->value;
    // }

    /**
     * Return a plain representation of the enum.
     *
     * This method is not meant to be called directly, but rather be called
     * by Laravel through a recursive serialization with @see \Illuminate\Contracts\Support\Arrayable.
     * Thus, it returns a value meant to be included in a plain array.
     */
    public function toArray(): mixed
    {
        return $this->value;
    }

    /**
     * Get the value for a single enum key.
     *
     * @param string $key
     *
     * @return mixed
     * @throws ReflectionException
     */
    public static function getValue(string $key): mixed
    {
        return static::getConstants()[$key];
    }

    /**
     * Returns the array of values.
     *
     * @return array
     * @throws ReflectionException
     */
    public static function values(): array
    {
        self::bootClass();

        return array_values(static::$cache[static::class]);
    }

    /**
     * Get the key for a single enum value.
     *
     * @param mixed $value
     *
     * @return string
     * @throws ReflectionException
     */
    public static function getKey(mixed $value): string
    {
        $retVal = array_search($value, static::getConstants(), true);

        return is_bool($retVal) ? '' : $retVal;
    }

    /**
     * Get the ordinal for a single enum key.
     *
     * @param mixed $key
     *
     * @return integer|null
     * @throws ReflectionException
     */
    public static function getOrdinal(mixed $key): int|null
    {
        return array_search($key, array_keys(static::getConstants()), true);
    }

    /**
     * Get the description for an enum value.
     *
     * @param mixed $value
     *
     * @return string|null
     * @throws ReflectionException
     * @throws Exception
     */
    public static function getDescription(mixed $value): ?string
    {
        self::bootClass();

        if (!is_null($value)) {
            return
                static::getLocalizedDescription($value) ??
                static::getAttributeDescription($value) ??
                static::getFriendlyKeyName(static::getKey($value));
        }

        return null;
    }

    /**
     * Get the localized description of a value.
     *
     * This works only if localization is enabled
     * for the enum and if the key exists in the lang file.
     *
     * @param mixed $value
     *
     * @return string|null
     */
    protected static function getLocalizedDescription(mixed $value): ?string
    {
        if (static::isLocalizable()) {
            $localizedStringKey = static::getLocalizationKey() . '.' . $value;

            if (Lang::has($localizedStringKey)) {
                return __($localizedStringKey);
            }
        }

        return null;
    }

    /**
     * Check that the enum implements the LocalizedEnum interface.
     *
     * @return boolean
     * @noinspection SpellCheckingInspection
     */
    protected static function isLocalizable(): bool
    {
        return isset(class_implements(static::class)[LocalizedEnum::class]);
    }

    /**
     * Get the default localization key.
     *
     * @return string
     */
    public static function getLocalizationKey(): string
    {
        return 'enums.' . static::class;
    }

    /**
     * Get the description of a value from its PHP attribute.
     *
     * @param mixed $value
     *
     * @return string|null
     * @throws Exception
     */
    protected static function getAttributeDescription(mixed $value): ?string
    {
        $reflection = self::getReflection();
        $constantName = static::getKey($value);
        $constReflection = $reflection->getReflectionConstant($constantName);

        if ($constReflection === false) {
            return null;
        }

        /** @var array<ReflectionAttribute> $descriptionAttributes */
        $descriptionAttributes = $constReflection->getAttributes(Description::class);

        if (empty($descriptionAttributes)) {
            return null;
        }

        if (count($descriptionAttributes) === 1) {
            return $descriptionAttributes[0]->newInstance()->getDescription();
        }

        if (count($descriptionAttributes) > 1) {
            throw new Exception(
                'You cannot use more than 1 description attribute on ' .
                class_basename(static::class) . '::' . $constantName
            );
        }

        return null;
    }

    /**
     * Transform the key name into a friendly, formatted version.
     *
     * @param string $key
     *
     * @return string
     */
    protected static function getFriendlyKeyName(string $key): string
    {
        if (ctype_upper(preg_replace('/[^a-zA-Z]/', '', $key))) {
            $key = strtolower($key);
        }

        return ucfirst(str_replace('_', ' ', Str::snake($key)));
    }

    /**
     * Get the label for an enum value.
     *
     * !!Make sure it only gets called after bootClass()!!
     *
     * @param mixed $value
     *
     * @return string|null
     * @throws ReflectionException
     * @throws Exception
     */
    public static function getLabel(mixed $value): ?string
    {
        self::bootClass();

        if (static::hasLabels() and isset(static::$labels[$value])) {
            return (string)static::$labels[$value];
        }

        return
            static::getLocalizedLabel($value) ??
            static::getAttributeLabel($value) ??
            (string) $value;
    }

    /**
     * Returns whether the labels property is defined on the actual class.
     *
     * @return boolean
     */
    private static function hasLabels(): bool
    {
        return property_exists(static::class, 'labels');
    }

    /**
     * Get the localized label of a value.
     *
     * This works only if localization is enabled
     * for the enum and if the key exists in the lang file.
     *
     * @param mixed $value
     *
     * @return string|null
     */
    protected static function getLocalizedLabel(mixed $value): ?string
    {
        if (static::isLocalizable()) {
            $localizedStringKey = static::getLocalizationKey() . '.' . $value;

            if (Lang::has($localizedStringKey)) {
                return __($localizedStringKey);
            }
        }

        return null;
    }

    /**
     * Get the label of a value from its PHP attribute.
     *
     * @param mixed $value
     *
     * @return string|null
     * @throws Exception
     */
    protected static function getAttributeLabel(mixed $value): ?string
    {
        $reflection = self::getReflection();
        $constantName = static::getKey($value);
        $constReflection = $reflection->getReflectionConstant($constantName);

        if ($constReflection === false) {
            return null;
        }

        /** @var array<ReflectionAttribute> $labelAttributes */
        $labelAttributes = $constReflection->getAttributes(Label::class);

        if (empty($labelAttributes)) {
            return null;
        }

        if (count($labelAttributes) === 1) {
            return $labelAttributes[0]->newInstance()->getLabel();
        }

        if (count($labelAttributes) > 1) {
            throw new Exception(
                'You cannot use more than 1 label attribute on ' .
                class_basename(static::class) . '::' . $constantName
            );
        }

        return null;
    }

    /**
     * Restores an enum instance exported by var_export().
     *
     * @param array{value: mixed, key: string, description: string} $enum
     *
     * @return static
     * @throws InvalidEnumMemberException
     * @throws ReflectionException
     */
    public static function __set_state(array $enum): static
    {
        return new static($enum['value']);
    }

    /**
     * Return instances of all the contained values.
     *
     * @return array<int, static>
     * @throws InvalidEnumMemberException
     * @throws ReflectionException
     */
    public static function getInstances(): array
    {
        return array_map(
            static function ($constantValue): static {
                return new static($constantValue);
            },
            static::getConstants()
        );
    }

    /**
     * Attempt to instantiate a new Enum using the given key or value.
     *
     * @param mixed $enumKeyOrValue
     *
     * @return static|null
     * @throws InvalidEnumMemberException
     * @throws ReflectionException
     */
    public static function coerce(mixed $enumKeyOrValue): ?Enum
    {
        if ($enumKeyOrValue === null) {
            return null;
        }

        if ($enumKeyOrValue instanceof static) {
            return $enumKeyOrValue;
        }

        if (static::hasValue($enumKeyOrValue)) {
            return static::fromValue($enumKeyOrValue);
        }

        if (is_string($enumKeyOrValue) and static::hasKey($enumKeyOrValue)) {
            $enumValue = static::getValue($enumKeyOrValue);

            return new static($enumValue);
        }

        return null;
    }

    /**
     * Make a new instance from an enum value.
     *
     * @param mixed $enumValue
     *
     * @return static
     * @throws InvalidEnumMemberException
     * @throws ReflectionException
     */
    public static function fromValue(mixed $enumValue): self
    {
        if ($enumValue instanceof static) {
            return $enumValue;
        }

        return new static($enumValue);
    }

    /**
     * Check that the enum contains a specific key.
     *
     * @param mixed $key
     *
     * @return boolean
     * @throws ReflectionException
     */
    public static function hasKey(mixed $key): bool
    {
        return in_array($key, static::getKeys(), true);
    }

    /**
     * Get all or a custom set of the enum keys.
     *
     * @param mixed ...$values
     *
     * @return array
     * @throws ReflectionException
     */
    public static function getKeys(mixed ...$values): array
    {
        $argc = count($values);

        /** @var array|mixed|null $parameters */
        $parameters = null;

        if ($argc === 1 and is_array($values[0])) {
            $parameters = $values[0];
        } elseif ($argc >= 1) {
            $parameters = $values;
        }

        if ($parameters === null) {
            return array_keys(static::getConstants());
        }

        return collect($parameters)->map(static fn($value) => static::getKey($value))->toArray();
    }

    /**
     * Get a random key from the enum.
     *
     * @return string
     * @throws ReflectionException
     */
    public static function getRandomKey(): string
    {
        $keys = static::getKeys();

        return $keys[array_rand($keys)];
    }

    /**
     * Get a random instance of the enum.
     *
     * @return static
     * @throws InvalidEnumMemberException
     * @throws ReflectionException
     */
    public static function getRandomInstance(): self
    {
        return new static(static::getRandomValue());
    }

    /**
     * Get a random value from the enum.
     *
     * @return mixed
     * @throws ReflectionException
     */
    public static function getRandomValue(): mixed
    {
        $values = static::getValues();

        return $values[array_rand($values)];
    }

    /**
     * Get the enum as an array formatted for a select.
     *
     * @return array<array-key, string>
     * @throws ReflectionException
     */
    public static function asSelectArray(): array
    {
        $array = static::asArray();
        $selectArray = [];

        foreach ($array as $key => $value) {
            $selectArray[$value] = static::getDescription($value);
        }

        return $selectArray;
    }

    /**
     * Return the enum as an array.
     *
     * @return array<string, mixed>
     * @throws ReflectionException
     */
    public static function asArray(): array
    {
        return static::getConstants() ?? [];
    }

    /**
     * Cast values loaded from the database before constructing an enum from them.
     *
     * You may need to overwrite this when using string values that are returned
     * from a raw database query or a similar data source.
     *
     * @param mixed $value A raw value that may have any native type
     *
     * @return mixed  The value cast into the type this enum expects
     */
    public static function parseDatabase(mixed $value): mixed
    {
        return $value;
    }

    /**
     * Transform value from the enum instance before it's persisted to the database.
     *
     * You may need to overwrite this when using a database that expects a different
     * type to that used internally in your enum.
     *
     * @param mixed $value A raw value that may have any native type
     *
     * @return mixed  The value cast into the type this database expects
     */
    public static function serializeDatabase(mixed $value): mixed
    {
        if ($value instanceof static) {
            return $value->value;
        }

        return $value;
    }

    /**
     * Get the name of the caster class to use when casting from / to this cast target.
     *
     * @param array $arguments
     *
     * @return CastsAttributes|EnumCast|string|CastsInboundAttributes
     */
    public static function castUsing(array $arguments): CastsAttributes|EnumCast|string|CastsInboundAttributes
    {
        return new EnumCast(static::class);
    }

    /**
     * Returns the array of labels.
     *
     * @return array
     * @throws ReflectionException
     */
    public static function labels(): array
    {
        self::bootClass();

        $result = [];

        foreach (static::values() as $value) {
            $result[] = static::getLabel($value);
        }

        return $result;
    }

    /**
     * Returns an array of value => label pairs.
     * Ready to pass to dropdowns.
     *
     * Example:
     * ```
     *   const Foo = 'Foo';
     *   const Bar = 'Bar'
     *
     *   protected static array $labels = [
     *     self::Foo => 'I am Foo',
     *     self::Bar => 'I am Bar'
     *   ];
     * ```
     *   self::choices returns:
     * ```
     *  [
     *    'Foo' => 'I am Foo',
     *    'Bar' => 'I am Bar'
     *  ]
     * ```
     *
     * @return array
     * @throws ReflectionException
     */
    public static function choices(): array
    {
        self::bootClass();

        $retVal = [];

        foreach (static::values() as $enumValue) {
            $retVal[$enumValue] = static::getLabel($enumValue);
        }

        return $retVal;
    }

    /**
     * Clears static class metadata. Mainly useful in testing environments.
     * Next time the enum class gets used, the class will be rebooted.
     *
     * @return void
     */
    public static function reset(): void
    {
        if (array_key_exists(static::class, static::$cache)) {
            unset(static::$cache[static::class]);
        }
    }

    /**
     * Returns the default value of the class. Equals to the Default constant.
     *
     * @return mixed
     */
    public static function defaultValue(): mixed
    {
        return static::Default;
    }

    /**
     * @return ArrayIterator|Iterator Enum items in order they are defined
     * @throws ReflectionException
     */
    final public static function iterator(): Iterator|ArrayIterator
    {
        return new ArrayIterator(static::getValues());
    }

    /**
     * Gets the SQL declaration snippet for a field of this type.
     *
     * @param boolean $quoted
     *
     * @return string
     * @noinspection PhpUnnecessaryCurlyVarSyntaxInspection
     * @throws ReflectionException
     */
    public static function toSQLDeclaration(bool $quoted = true): string
    {
        $quote = $quoted ? '\'' : '';

        $keys = array_filter(
            static::getKeys(),
            static fn(string $item) => !in_array($item, ['None', 'Default'])
        );

        $retVal = implode(
            ',',
            array_map(static fn(string $item) => "${quote}$item${quote}", $keys)
        );

        return "ENUM(${retVal})";
    }

    /**
     * Check if is valid enum value
     *
     * @param mixed $value
     *
     * @psalm-param mixed $value
     * @psalm-pure
     * @psalm-assert-if-true T $value
     * @return boolean
     * @throws ReflectionException
     */
    public static function isValid(mixed $value): bool
    {
        return in_array($value, static::asArray(), true);
    }

    /**
     * Check if is valid enum key
     *
     * @param mixed $key
     *
     * @psalm-param string $key
     * @psalm-pure
     * @return boolean
     * @throws ReflectionException
     */
    public static function isValidKey(mixed $key): bool
    {
        $array = static::asArray();

        return isset($array[$key]) || array_key_exists($key, $array);
    }

    /**
     * Delegate magic method calls to macro's or the static call.
     *
     * While it is not typical to use the magic instantiation dynamically, it may happen
     * incidentally when calling the instantiation in an instance method of itself.
     * Even when using the `static::KEY()` syntax, PHP still interprets this is a call to
     * an instance method when it happens inside an instance method of the same class.
     *
     * @param string $method
     * @param mixed $parameters
     *
     * @return mixed
     * @throws InvalidEnumKeyException
     * @throws InvalidEnumMemberException
     * @throws ReflectionException
     */
    public function __call($method, $parameters)
    {
        if (static::hasMacro($method)) {
            return $this->macroCall($method, $parameters);
        }

        if (str_starts_with($method, 'is') and strlen($method) > 2 and ctype_upper($method[2])) {
            $constName = static::stringToConstName(substr($method, 2));

            if (static::hasConstant($constName)) {
                return $this->equalsByConstantName($constName);
            }
        } elseif (str_starts_with($method, 'to') and strlen($method) > 2 and ctype_upper($method[2])) {
            $methodName = substr($method, 2);

            if ($methodName === 'String') {
                return (string)$this->value;
            }
        } /*elseif (str_starts_with($method, 'get') and strlen($method) > 3 and ctype_upper($method[3])) {
            $methodName = substr($method, 3);

            if ($methodName === 'Value') {
                return $this->value;
            } else if ($methodName === 'Key') {
                return $this->key;
            } else if ($methodName === 'Ordinal') {
                return $this->ordinal;
            }
        }*/

        if ($method === 'key') {
            return $this->key;
        } else if ($method === 'value') {
            return $this->value;
        } else if ($method === 'ordinal') {
            return $this->ordinal;
        } else if ($method === 'description') {
            return $this->description;
        } else if ($method === 'label') {
            return static::getLabel($this->value) ?? '';
        }

        if (!static::hasKey($method)) {
            trigger_error(
                sprintf('Call to undefined method: %s::%s()', static::class, $method),
                E_USER_WARNING
            );
        }

        return self::__callStatic($method, $parameters);
    }

    /**
     * Convert the given `string` into constant name.
     *
     * @param string $string String to convert.
     *
     * @return string
     */
    protected static function stringToConstName(string $string): string
    {
        if (!ctype_lower($string)) {
            $string = preg_replace('/\s+/u', '', ucwords($string));
            $string = strtolower(preg_replace('/(.)(?=[A-Z])/u', '$1' . '_', $string));
        }

        return strtoupper($string);
    }

    /**
     * Returns whether a const is present in the specific enum class.
     *
     * @param string $constant
     *
     * @return boolean
     * @throws ReflectionException
     */
    public static function hasConstant(string $constant): bool
    {
        return in_array($constant, static::getConstants());
    }

    /**
     * Returns whether the enum instance equals with a value of the same
     * type created from the given const name
     *
     * @param string $constant
     *
     * @return boolean
     * @throws ReflectionException
     * @throws InvalidEnumMemberException
     */
    protected function equalsByConstantName(string $constant): bool
    {
        return $this->equals(static::create(constant(static::class . '::' . $constant)));
    }

    /**
     * Checks if two enums are equal. Value and class are both matched.
     * Value check is not type strict.
     *
     * @param mixed $object
     *
     * @return boolean True if enums are equal
     */
    public function equals(mixed $object): bool
    {
        if (!($object instanceof static) or !static::compatibles(get_class($object), static::class)) {
            return false;
        }

        return $this->value === $object->value;
    }

    /**
     * Returns whether two enum classes are compatible (are the same type or one descends from the other).
     *
     * @param string $lhs
     * @param string $rhs
     *
     * @return boolean
     */
    protected static function compatibles(string $lhs, string $rhs): bool
    {
        if ($lhs === $rhs) {
            return true;
        } elseif (is_subclass_of($lhs, $rhs)) {
            return true;
        } elseif (is_subclass_of($rhs, $lhs)) {
            return true;
        }

        return false;
    }

    /**
     * Factory method for creating instance.
     *
     * @param mixed|null $value The value for the instance
     *
     * @return static
     * @throws ReflectionException
     * @throws InvalidEnumMemberException
     */
    public static function create(mixed $value = null): static
    {
        return new static($value);
    }

    /**
     * Attempt to instantiate an enum by calling the enum key as a static method.
     *
     * This function defers to the macroable __callStatic function if a macro is found using the static
     * method called.
     *
     * @param string $method
     * @param mixed $parameters
     *
     * @return mixed
     * @throws InvalidEnumKeyException
     * @throws InvalidEnumMemberException
     * @throws ReflectionException
     * @noinspection PhpUnnecessaryCurlyVarSyntaxInspection
     */
    public static function __callStatic($method, $parameters)
    {
        if (static::hasMacro($method)) {
            return static::macroCallStatic($method, $parameters);
        }

        return clone static::fromKey($method);
    }

    /**
     * Make an enum instance from a given key.
     *
     * @param string $key
     *
     * @return static
     *
     * @throws InvalidEnumKeyException
     * @throws InvalidEnumMemberException
     * @throws ReflectionException
     */
    public static function fromKey(string $key): self
    {
        if (static::hasKey($key)) {
            $enumValue = static::getValue($key);

            return new static($enumValue);
        }

        throw new InvalidEnumKeyException($key, static::class);
    }

    // /**
    //  * Returns a single member by comparison with the result of an accessor
    //  * method.
    //  *
    //  * @param string $property The name of the property (accessor method) to match.
    //  * @param mixed $value The value to match.
    //  * @param boolean|null $isCaseSensitive True if the search should be case sensitive.
    //  *
    //  * @return static                            The first member for which $member->{$property}() === $value.
    //  * @throws UndefinedEnumMemberException
    //  * @throws ReflectionException
    //  * @api
    //  */
    // protected static function memberBy(string $property, mixed $value, ?bool $isCaseSensitive = null): self
    // {
    //     $member = static::memberByWithDefault(
    //         $property,
    //         $value,
    //         null,
    //         $isCaseSensitive
    //     );
    //
    //     if (null === $member) {
    //         throw static::createUndefinedMemberException(
    //             get_called_class(),
    //             $property,
    //             $value
    //         );
    //     }
    //
    //     return $member;
    // }
    //
    // /**
    //  * Returns a single member by comparison with the result of an accessor
    //  * method. Additionally returns a default if no associated member is found.
    //  *
    //  * @param string $property The name of the property (accessor method) to match.
    //  * @param mixed $value The value to match.
    //  * @param Enum|null $default The default value to return.
    //  * @param boolean|null $isCaseSensitive True if the search should be case sensitive.
    //  *
    //  * @return static|null The first member for which $member->{$property}() === $value, or the default value.
    //  * @throws UndefinedEnumMemberException
    //  * @throws ReflectionException
    //  * @api
    //  */
    // protected static function memberByWithDefault(
    //     string $property,
    //     mixed $value,
    //     ?Enum $default = null,
    //     ? bool $isCaseSensitive = null
    // ): ?self {
    //     if (null === $isCaseSensitive) {
    //         $isCaseSensitive = true;
    //     }
    //
    //     if (!$isCaseSensitive and is_scalar($value)) {
    //         $value = strtoupper(strval($value));
    //     }
    //
    //     return static::memberByPredicateWithDefault(
    //         function ($member) use ($property, $value, $isCaseSensitive): bool {
    //             if (!is_null($property)) {
    //                 $memberValue = $member->{$property}();
    //
    //                 if (!$isCaseSensitive and is_scalar($memberValue)) {
    //                     $memberValue = strtoupper(strval($memberValue));
    //                 }
    //
    //                 return $memberValue === $value;
    //             }
    //
    //             return false;
    //         },
    //         $default
    //     );
    // }
    //
    // /**
    //  * Returns a single member by predicate callback. Additionally returns a
    //  * default if no associated member is found.
    //  *
    //  * @param callable $predicate The predicate applied to the member to find a match.
    //  * @param Enum|null $default The default value to return.
    //  *
    //  * @return Enum|null The first member for which $predicate($member) evaluates to boolean true, or the
    //  *     default value.
    //  *
    //  * @throws ReflectionException
    //  *
    //  * @api
    //  */
    // protected static function memberByPredicateWithDefault(callable $predicate, ?Enum $default = null): ?self
    // {
    //     foreach (static::asArray() as $member) {
    //         if ($predicate($member)) {
    //             return $member;
    //         }
    //     }
    //
    //     return $default;
    // }
    //
    // /**
    //  * Override this method in child classes to implement custom undefined
    //  * member exceptions.
    //  *
    //  * @param string $className The name of the class from which the member was requested.
    //  * @param string $property The name of the property used to search for the member.
    //  * @param mixed $value The value of the property used to search for the member.
    //  * @param Exception|null $cause The cause, if available.
    //  *
    //  * @return UndefinedEnumMemberException The newly created exception.
    //  * @api
    //  */
    // protected static function createUndefinedMemberException(
    //     string $className,
    //     string $property,
    //     mixed $value,
    //     ?Exception $cause = null
    // ): UndefinedEnumMemberException {
    //     return new UndefinedEnumMemberException(
    //         $className,
    //         $property,
    //         $value,
    //         $cause
    //     );
    // }
    //
    // /**
    //  * Get Enumeration by the given `prediction`.
    //  *
    //  * @param mixed|null $predict
    //  * @param boolean $isCaseSensitive
    //  *
    //  * @return static|null
    //  */
    // public static function by(mixed $predict = null, bool $isCaseSensitive = false): ?self
    // {
    //     $type = null;
    //     $result = null;
    //
    //     if ($predict instanceof Enum) {
    //         $predict = $predict->label();
    //         $type = 'label';
    //     }
    //
    //     if (is_string($predict)) {
    //         $type = 'label';
    //     } elseif (is_numeric($predict) or is_integer($predict)) {
    //         $type = 'value';
    //     }
    //
    //     try {
    //         $result = static::memberBy($type, $predict, $isCaseSensitive);
    //     } catch (ReflectionException | UndefinedEnumMemberException $ex) {
    //         return null;
    //     } finally {
    //         return $result;
    //     }
    // }
    //
    // /**
    //  * Get Enumeration by the given `name`.
    //  *
    //  * @param string|null $name
    //  * @param boolean|null $isCaseSensitive
    //  *
    //  * @return static|null
    //  */
    // public static function byName(string $name = null, ?bool $isCaseSensitive = false): ?self
    // {
    //     $result = null;
    //
    //     try {
    //         $result = static::memberBy('label', $name, $isCaseSensitive);
    //     } catch (UndefinedEnumMemberException | ReflectionException $ex) {
    //         return null;
    //     } finally {
    //         return $result;
    //     }
    // }
    //
    // /**
    //  * Get Enumeration by the given `value`.
    //  *
    //  * @param mixed $value
    //  * @param boolean|null $isCaseSensitive
    //  *
    //  * @return static|null
    //  */
    // public static function byValue(mixed $value = null, ?bool $isCaseSensitive = false): ?self
    // {
    //     $result = null;
    //
    //     try {
    //         $result = static::memberBy('value', $value, $isCaseSensitive);
    //     } catch (UndefinedEnumMemberException | ReflectionException $ex) {
    //         return null;
    //     } finally {
    //         return $result;
    //     }
    // }
    //
    // /**
    //  * Get Enumeration by the given `ordinal`.
    //  *
    //  * @param integer|null $ordinal
    //  *
    //  * @return static|null
    //  */
    // public static function byOrdinal(?int $ordinal = null): ?self
    // {
    //     $result = null;
    //
    //     try {
    //         $result = static::memberBy('ordinal', $ordinal, false);
    //     } catch (UndefinedEnumMemberException | ReflectionException $ex) {
    //         return null;
    //     } finally {
    //         return $result;
    //     }
    // }

    /**
     * Checks if this instance is not equal to the given enum instance or value.
     *
     * @param static|mixed $enumValue
     *
     * @return boolean
     */
    public function isNot(mixed $enumValue): bool
    {
        return !$this->is($enumValue);
    }

    /**
     * Checks if this instance is equal to the given enum instance or value.
     *
     * @param static|mixed $enumValue
     *
     * @return boolean
     */
    public function is(mixed $enumValue): bool
    {
        if ($enumValue instanceof static) {
            return $this->value === $enumValue->value;
        }

        return $this->value === $enumValue;
    }

    /**
     * Checks if a matching enum instance or value is in the given array.
     *
     * @param iterable $values
     *
     * @return boolean
     */
    public function in(iterable $values): bool
    {
        foreach ($values as $value) {
            if ($this->is($value)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks if a matching enum instance or value is not in the given array.
     *
     * @param iterable $values
     *
     * @return boolean
     */
    public function notIn(iterable $values): bool
    {
        foreach ($values as $value) {
            if ($this->is($value)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Checks if two enums are NOT equal. Value and class are both matched.
     * Value check is not type strict.
     *
     * @param mixed $object
     *
     * @return boolean True if enums do not equal
     */
    public function notEquals(mixed $object): bool
    {
        return !$this->equals($object);
    }

    /**
     * This method exists only for the compatibility reason when deserializing a previously serialized
     * version that didn't had the key property
     *
     * @throws ReflectionException
     */
    public function __wakeup()
    {
        /** @psalm-suppress DocblockTypeContradiction key can be null when deserializing an enum without the key */
        if ($this->key === null) {
            /**
             * @psalm-suppress InaccessibleProperty key is not readonly as marked by psalm
             * @psalm-suppress PossiblyFalsePropertyAssignmentValue deserializing a case that was removed
             */
            $this->key = static::search($this->value);
        }
    }

    /**
     * Return key for value
     *
     * @param mixed $value
     *
     * @psalm-param mixed $value
     * @psalm-pure
     * @return string|false
     * @throws ReflectionException
     */
    public static function search(mixed $value): bool|string
    {
        return array_search($value, static::asArray(), true);
    }

    /**
     * Specify data which should be serialized to JSON. This method returns data that can be serialized by
     * json_encode() natively.
     *
     * @return mixed
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @psalm-pure
     */
    #[ReturnTypeWillChange]
    public function jsonSerialize(): mixed
    {
        return $this->value;
    }

    /**
     * Return a string representation of the enum.
     */
    public function __toString(): string
    {
        return (string)$this->value;
    }

    // /**
    //  * Return a string representation of the enum.
    //  */
    // public function toString(): string
    // {
    //     return (string)$this->value;
    // }

    // /**
    //  * Debug Information.
    //  *
    //  * @return array|null
    //  */
    // public function __debugInfo(): ?array
    // {
    //     return [
    //         $this->key => $this->value,
    //     ];
    // }
}

# vim: set ts=4 sw=4 tw=80 noet :
