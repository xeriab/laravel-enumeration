<?php /** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * EnumContract
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Contracts;

/**
 * EnumContract Interface.
 *
 * @package Exen\Laravel\Enumeration\Contracts
 */
interface EnumContract
{
    /**
     * Determine if this instance is equivalent to a given value.
     *
     * @param mixed $enumValue
     *
     * @return boolean
     */
    public function is(mixed $enumValue): bool;
}

# vim: set ts=4 sw=4 tw=80 noet :
