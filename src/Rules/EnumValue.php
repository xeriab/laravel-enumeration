<?php /** @noinspection PhpUnnecessaryCurlyVarSyntaxInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * Enum
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Rules;

use Exen\Laravel\Enumeration\FlaggedEnum;
use Illuminate\Contracts\Validation\Rule;
use InvalidArgumentException;
use ReflectionException;
use function __;
use function class_exists;
use function ctype_digit;
use function is_integer;
use function is_subclass_of;
use function trans;

/**
 * EnumValue Class.
 *
 * @package Exen\Laravel\Enumeration\Rules
 */
class EnumValue implements Rule
{
    /**
     * The name of the rule.
     *
     * @var string $rule
     */
    protected string $rule = 'enum_value';

    /**
     * The name of the Enumeration class.
     *
     * @var string|\Exen\Laravel\Enumeration\Enum $enumClass
     */
    protected string|\Exen\Laravel\Enumeration\Enum $enumClass;

    /**
     * @var boolean $strict
     */
    protected bool $strict;

    /**
     * Create a new rule instance.
     *
     * @param string $enumClass
     * @param bool $strict
     *
     * @return void
     *
     * @throws InvalidArgumentException
     */
    public function __construct(string $enumClass, bool $strict = true)
    {
        $this->enumClass = $enumClass;
        $this->strict = $strict;

        if (!class_exists($this->enumClass)) {
            throw new InvalidArgumentException("Cannot validate against the enum, the class {$this->enumClass} doesn't exist.");
        }
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     *
     * @return boolean
     * @throws ReflectionException
     */
    public function passes($attribute, $value): bool
    {
        if (is_subclass_of($this->enumClass, FlaggedEnum::class) && (is_integer($value) || ctype_digit($value))) {
            // Unset all possible flag values
            foreach ($this->enumClass::getValues() as $enumValue) {
                $value &= ~$enumValue;
            }

            // All bits should be unset
            return $value === 0;
        }

        return $this->enumClass::hasValue($value, $this->strict);
    }

    /**
     * Get the validation error message.
     *
     * @return string|array
     */
    public function message(): array|string
    {
        return trans()->has('validation.enum_value')
            ? __('validation.enum_value')
            : __('exen-laravel-enumeration::messages.enum_value');
    }

    /**
     * Convert the rule to a validation string.
     *
     * @return string
     *
     * @see \Illuminate\Validation\ValidationRuleParser::parseParameters
     */
    public function __toString()
    {
        $strict = $this->strict ? 'true' : 'false';

        return "{$this->rule}:{$this->enumClass},{$strict}";
    }
}

# vim: set ts=4 sw=4 tw=80 noet :
