<?php /** @noinspection PhpUnnecessaryCurlyVarSyntaxInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * Enum
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Rules;

use Illuminate\Contracts\Validation\Rule;
use InvalidArgumentException;
use ReflectionException;
use function __;
use function class_exists;
use function trans;

/**
 * EnumKey Class.
 *
 * @package Exen\Laravel\Enumeration\Rules
 */
class EnumKey implements Rule
{
    /**
     * The name of the rule.
     *
     * @var string $rule
     */
    protected string $rule = 'enum_key';

    /**
     * The name of enumeration class.
     *
     * @var string|\Exen\Laravel\Enumeration\Enum
     */
    protected string|\Exen\Laravel\Enumeration\Enum $enumClass;

    /**
     * Create a new rule instance.
     *
     * @param string $enum
     *
     * @return void
     *
     * @throws InvalidArgumentException
     */
    public function __construct(string $enum)
    {
        $this->enumClass = $enum;

        if (!class_exists($this->enumClass)) {
            throw new InvalidArgumentException("Cannot validate against the enum, the class {$this->enumClass} doesn't exist.");
        }
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     *
     * @return boolean
     * @throws ReflectionException
     */
    public function passes($attribute, $value): bool
    {
        return $this->enumClass::hasKey($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string|array
     */
    public function message(): array|string
    {
        return trans()->has('validation.enum_key')
            ? __('validation.enum_key')
            : __('exen-laravel-enumeration::messages.enum_key');
    }

    /**
     * Convert the rule to a validation string.
     *
     * @return string
     *
     * @see \Illuminate\Validation\ValidationRuleParser::parseParameters
     */
    public function __toString()
    {
        return "{$this->rule}:{$this->enumClass}";
    }
}

# vim: set ts=4 sw=4 tw=80 noet :
