<?php /** @noinspection PhpMultipleClassDeclarationsInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * Description
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Attributes;

use Attribute;

/**
 * Label Class.
 *
 * @attribute
 *
 * @package Exen\Laravel\Enumeration\Attributes
 */
#[Attribute(Attribute::TARGET_CLASS_CONSTANT)]
class Label
{
    /**
     * Label Attribute Constructor.
     *
     * @param string|null $label Label.
     */
    public function __construct(public ?string $label = null)
    {
        //
    }

    /**
     * Get Label.
     *
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * Set Label.
     *
     * @param string|null $label Label to set.
     *
     * @return $this
     */
    public function setLabel(?string $label = null): self
    {
        $this->label = $label;

        return $this;
    }
}

# vim: set ts=4 sw=4 tw=80 noet :
