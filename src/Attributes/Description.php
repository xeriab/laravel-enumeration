<?php /** @noinspection PhpMultipleClassDeclarationsInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * Description
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Attributes;

use Attribute;

/**
 * Description Class.
 *
 * @attribute
 *
 * @package Exen\Laravel\Enumeration\Attributes
 */
#[Attribute(Attribute::TARGET_CLASS_CONSTANT | Attribute::TARGET_CLASS)]
class Description
{
    /**
     * Description Attribute Constructor.
     *
     * @param string|null $description Description.
     */
    public function __construct(public ?string $description = null)
    {
        //
    }

    /**
     * Get Description.
     *
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Set Description.
     *
     * @param string|null $description Description to set.
     *
     * @return $this
     */
    public function setDescription(?string $description = null): self
    {
        $this->description = $description;

        return $this;
    }
}

# vim: set ts=4 sw=4 tw=80 noet :
