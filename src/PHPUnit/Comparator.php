<?php /** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * Comparator
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\PHPUnit;

use Exen\Laravel\Enumeration\Enum;
use SebastianBergmann\Comparator\Comparator as BaseComparator;
use SebastianBergmann\Comparator\ComparisonFailure;
use function get_class;

/**
 * Comparator Class.
 *
 * Use this Comparator to get nice output when using PHPUnit assertEquals() with Enums.
 *
 * Add this to your PHPUnit bootstrap PHP file:
 *
 * \SebastianBergmann\Comparator\Factory::getInstance()->register(new
 * \Exen\Laravel\Enumeration\PHPUnit\Comparator());
 *
 * @package Exen\Laravel\Enumeration\PHPUnit
 */
class Comparator extends BaseComparator
{
    /**
     * @param $expected
     * @param $actual
     *
     * @return boolean
     */
    public function accepts($expected, $actual): bool
    {
        return $expected instanceof Enum && ($actual instanceof Enum || $actual === null);
    }

    /**
     * {@inheritDoc}
     */
    public function assertEquals(
        $expected,
        $actual = null,
        $delta = 0.0,
        $canonicalize = false,
        $ignoreCase = false
    ): void {
        if ($expected->equals($actual)) {
            return;
        }

        throw new ComparisonFailure(
            $expected,
            $actual,
            $this->formatEnum($expected),
            $this->formatEnum($actual),
            false,
            'Failed asserting that two Enums are equal.'
        );
    }

    /**
     * Format Enum.
     *
     * @param Enum|null $enum
     *
     * @return string
     *
     * @noinspection PhpUnnecessaryCurlyVarSyntaxInspection
     */
    private function formatEnum(?Enum $enum = null): string
    {
        if ($enum === null) {
            return 'null';
        }

        return get_class($enum) . "::{$enum->key}()";
    }
}

# vim: set ts=4 sw=4 tw=80 noet :
