<?php /** @noinspection PhpUnusedAliasInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * Day
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Enums;

use Exen\Laravel\Enumeration\FlaggedEnum;
use Exen\Laravel\Enumeration\Contracts\LocalizedEnum;

/**
 * Day Enum.
 *
 * @method static Day Monday()
 * @method static Day Tuesday()
 * @method static Day Wednesday()
 * @method static Day Thursday()
 * @method static Day Friday()
 * @method static Day Saturday()
 * @method static Day Sunday()
 * @method static Day Weekdays()
 * @method static Day Weekend()
 *
 * Example:
 * ```php
 *  $activeDays = new Day([Day::Monday, Day::Wednesday, Day::Friday]);
 *
 *  $activeDays; // 21
 *  $activeDays->hasFlag(Day::Monday); // true
 *  $activeDays->hasFlag(Day::Tuesday); // false
 *
 *  $activeDays->getFlags(); // [Day::Monday(), Day::Wednesday(), Day::Friday()]
 *
 *  // Shortcuts
 *  Day::Weekend; // 112
 *  Day::Weekend()->getFlags(); // [Day::Friday(), Day::Saturday(), Day::Sunday(), Day::Weekend()]
 * ```
 *
 * @package Exen\Laravel\Enumeration\Enums
 */
final class Day extends FlaggedEnum implements LocalizedEnum
{
    const Monday    = 1 << 0;
    const Tuesday   = 1 << 1;
    const Wednesday = 1 << 2;
    const Thursday  = 1 << 3;
    const Friday    = 1 << 4;
    const Saturday  = 1 << 5;
    const Sunday    = 1 << 6;

    // Shortcuts
    const Weekdays = self::Monday | self::Tuesday | self::Wednesday | self::Thursday | self::Friday;
    const Weekend = self::Saturday | self::Sunday | self::Friday;
}

# vim: set ts=4 sw=4 tw=80 noet :
