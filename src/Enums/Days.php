<?php /** @noinspection PhpUnusedAliasInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * Days
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Enums;

use Exen\Laravel\Enumeration\Enum;
use Exen\Laravel\Enumeration\Contracts\LocalizedEnum;

/**
 * Days Enum.
 *
 * @method static Days Monday()
 * @method static Days Tuesday()
 * @method static Days Wednesday()
 * @method static Days Thursday()
 * @method static Days Friday()
 * @method static Days Saturday()
 * @method static Days Sunday()
 *
 * @package Exen\Laravel\Enumeration\Enums
 */
final class Days extends Enum implements LocalizedEnum
{
    const Monday    = 0x1;
    const Tuesday   = 0x2;
    const Wednesday = 0x3;
    const Thursday  = 0x4;
    const Friday    = 0x5;
    const Saturday  = 0x6;
    const Sunday    = 0x7;
}

# vim: set ts=4 sw=4 tw=80 noet :
