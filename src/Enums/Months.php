<?php /** @noinspection PhpUnusedAliasInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * Months
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Enums;

use Exen\Laravel\Enumeration\Enum;

/**
 * Months Enum.
 *
 * @method static Months January()
 * @method static Months February()
 * @method static Months March()
 * @method static Months April()
 * @method static Months May()
 * @method static Months June()
 * @method static Months July()
 * @method static Months August()
 * @method static Months September()
 * @method static Months October()
 * @method static Months November()
 * @method static Months December()
 *
 * @package Exen\Laravel\Enumeration\Enums
 */
final class Months extends Enum
{
    const January   = 0x1;
    const February  = 0x2;
    const March     = 0x3;
    const April     = 0x4;
    const May       = 0x5;
    const June      = 0x6;
    const July      = 0x7;
    const August    = 0x8;
    const September = 0x9;
    const October   = 0x10;
    const November  = 0x11;
    const December  = 0x12;
}

# vim: set ts=4 sw=4 tw=80 noet :
