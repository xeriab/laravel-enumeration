<?php /** @noinspection PhpUnusedAliasInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * LogLevel
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Enums;

use Exen\Laravel\Enumeration\Enum;

/**
 * LogLevel Enum.
 *
 * @method static LogLevel Emergency()
 * @method static LogLevel Alert()
 * @method static LogLevel Critical()
 * @method static LogLevel Error()
 * @method static LogLevel Warning()
 * @method static LogLevel Notice()
 * @method static LogLevel Info()
 * @method static LogLevel Debug()
 *
 * @package Exen\Laravel\Enumeration\Enums
 */
final class LogLevel extends Enum
{
    const Emergency = 0x0;
    const Alert     = 0x1;
    const Critical  = 0x2;
    const Error     = 0x3;
    const Warning   = 0x4;
    const Notice    = 0x5;
    const Info      = 0x6;
    const Debug     = 0x7;
}

# vim: set ts=4 sw=4 tw=80 noet :
