<?php /** @noinspection SpellCheckingInspection */
/** @noinspection PhpUnusedAliasInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * Countries
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Enums;

use Exen\Laravel\Enumeration\Enum;
use Exen\Laravel\Enumeration\Contracts\LocalizedEnum;

/**
 * Countries Enum.
 *
 * @method static Countries AF()
 * @method static Countries AL()
 * @method static Countries DZ()
 * @method static Countries AS()
 * @method static Countries AD()
 * @method static Countries AO()
 * @method static Countries AI()
 * @method static Countries AQ()
 * @method static Countries AG()
 * @method static Countries AR()
 * @method static Countries AM()
 * @method static Countries AW()
 * @method static Countries AU()
 * @method static Countries AT()
 * @method static Countries AZ()
 * @method static Countries BS()
 * @method static Countries BH()
 * @method static Countries BD()
 * @method static Countries BB()
 * @method static Countries BY()
 * @method static Countries BE()
 * @method static Countries BZ()
 * @method static Countries BJ()
 * @method static Countries BM()
 * @method static Countries BT()
 * @method static Countries BO()
 * @method static Countries BA()
 * @method static Countries BW()
 * @method static Countries BV()
 * @method static Countries BR()
 * @method static Countries IO()
 * @method static Countries BN()
 * @method static Countries BG()
 * @method static Countries BF()
 * @method static Countries BI()
 * @method static Countries KH()
 * @method static Countries CM()
 * @method static Countries CA()
 * @method static Countries CV()
 * @method static Countries KY()
 * @method static Countries CF()
 * @method static Countries TD()
 * @method static Countries CL()
 * @method static Countries CN()
 * @method static Countries CX()
 * @method static Countries CC()
 * @method static Countries CO()
 * @method static Countries KM()
 * @method static Countries CG()
 * @method static Countries CD()
 * @method static Countries CK()
 * @method static Countries CR()
 * @method static Countries CI()
 * @method static Countries HR()
 * @method static Countries CU()
 * @method static Countries CY()
 * @method static Countries CZ()
 * @method static Countries DK()
 * @method static Countries DJ()
 * @method static Countries DM()
 * @method static Countries DO()
 * @method static Countries EC()
 * @method static Countries EG()
 * @method static Countries SV()
 * @method static Countries GQ()
 * @method static Countries ER()
 * @method static Countries EE()
 * @method static Countries ET()
 * @method static Countries FK()
 * @method static Countries FO()
 * @method static Countries FJ()
 * @method static Countries FI()
 * @method static Countries FR()
 * @method static Countries GF()
 * @method static Countries PF()
 * @method static Countries TF()
 * @method static Countries GA()
 * @method static Countries GM()
 * @method static Countries GE()
 * @method static Countries DE()
 * @method static Countries GH()
 * @method static Countries GI()
 * @method static Countries GR()
 * @method static Countries GL()
 * @method static Countries GD()
 * @method static Countries GP()
 * @method static Countries GU()
 * @method static Countries GT()
 * @method static Countries GN()
 * @method static Countries GW()
 * @method static Countries GY()
 * @method static Countries HT()
 * @method static Countries HM()
 * @method static Countries VA()
 * @method static Countries HN()
 * @method static Countries HK()
 * @method static Countries HU()
 * @method static Countries IS()
 * @method static Countries IN()
 * @method static Countries ID()
 * @method static Countries IR()
 * @method static Countries IQ()
 * @method static Countries IE()
 * @method static Countries IL()
 * @method static Countries IT()
 * @method static Countries JM()
 * @method static Countries JP()
 * @method static Countries JO()
 * @method static Countries KZ()
 * @method static Countries KE()
 * @method static Countries KI()
 * @method static Countries KP()
 * @method static Countries KR()
 * @method static Countries KW()
 * @method static Countries KG()
 * @method static Countries LA()
 * @method static Countries LV()
 * @method static Countries LB()
 * @method static Countries LS()
 * @method static Countries LR()
 * @method static Countries LY()
 * @method static Countries LI()
 * @method static Countries LT()
 * @method static Countries LU()
 * @method static Countries MO()
 * @method static Countries MK()
 * @method static Countries MG()
 * @method static Countries MW()
 * @method static Countries MY()
 * @method static Countries MV()
 * @method static Countries ML()
 * @method static Countries MT()
 * @method static Countries MH()
 * @method static Countries MQ()
 * @method static Countries MR()
 * @method static Countries MU()
 * @method static Countries YT()
 * @method static Countries MX()
 * @method static Countries FM()
 * @method static Countries MD()
 * @method static Countries MC()
 * @method static Countries MN()
 * @method static Countries MS()
 * @method static Countries MA()
 * @method static Countries MZ()
 * @method static Countries MM()
 * @method static Countries NA()
 * @method static Countries NR()
 * @method static Countries NP()
 * @method static Countries NL()
 * @method static Countries AN()
 * @method static Countries NC()
 * @method static Countries NZ()
 * @method static Countries NI()
 * @method static Countries NE()
 * @method static Countries NG()
 * @method static Countries NU()
 * @method static Countries NF()
 * @method static Countries MP()
 * @method static Countries NO()
 * @method static Countries OM()
 * @method static Countries PK()
 * @method static Countries PW()
 * @method static Countries PS()
 * @method static Countries PA()
 * @method static Countries PG()
 * @method static Countries PY()
 * @method static Countries PE()
 * @method static Countries PH()
 * @method static Countries PN()
 * @method static Countries PL()
 * @method static Countries PT()
 * @method static Countries PR()
 * @method static Countries QA()
 * @method static Countries RE()
 * @method static Countries RO()
 * @method static Countries RU()
 * @method static Countries RW()
 * @method static Countries SH()
 * @method static Countries KN()
 * @method static Countries LC()
 * @method static Countries PM()
 * @method static Countries VC()
 * @method static Countries WS()
 * @method static Countries SM()
 * @method static Countries ST()
 * @method static Countries SA()
 * @method static Countries SN()
 * @method static Countries CS()
 * @method static Countries SC()
 * @method static Countries SL()
 * @method static Countries SG()
 * @method static Countries SK()
 * @method static Countries SI()
 * @method static Countries SB()
 * @method static Countries SO()
 * @method static Countries ZA()
 * @method static Countries GS()
 * @method static Countries ES()
 * @method static Countries LK()
 * @method static Countries SD()
 * @method static Countries SR()
 * @method static Countries SJ()
 * @method static Countries SZ()
 * @method static Countries SE()
 * @method static Countries CH()
 * @method static Countries SY()
 * @method static Countries TW()
 * @method static Countries TJ()
 * @method static Countries TZ()
 * @method static Countries TH()
 * @method static Countries TL()
 * @method static Countries TG()
 * @method static Countries TK()
 * @method static Countries TO()
 * @method static Countries TT()
 * @method static Countries TN()
 * @method static Countries TR()
 * @method static Countries TM()
 * @method static Countries TC()
 * @method static Countries TV()
 * @method static Countries UG()
 * @method static Countries UA()
 * @method static Countries AE()
 * @method static Countries GB()
 * @method static Countries US()
 * @method static Countries UM()
 * @method static Countries UY()
 * @method static Countries UZ()
 * @method static Countries VU()
 * @method static Countries VE()
 * @method static Countries VN()
 * @method static Countries VG()
 * @method static Countries VI()
 * @method static Countries WF()
 * @method static Countries EH()
 * @method static Countries YE()
 * @method static Countries ZM()
 * @method static Countries ZW()
 *
 * @package Exen\Laravel\Enumeration\Enums
 */
final class Countries extends Enum implements LocalizedEnum
{
    const AF = 'Afghanistan';
    const AL = 'Albania';
    const DZ = 'Algeria';
    const AS = 'American Samoa';
    const AD = 'Andorra';
    const AO = 'Angola';
    const AI = 'Anguilla';
    const AQ = 'Antarctica';
    const AG = 'Antigua and Barbuda';
    const AR = 'Argentina';
    const AM = 'Armenia';
    const AW = 'Aruba';
    const AU = 'Australia';
    const AT = 'Austria';
    const AZ = 'Azerbaijan';
    const BS = 'Bahamas';
    const BH = 'Bahrain';
    const BD = 'Bangladesh';
    const BB = 'Barbados';
    const BY = 'Belarus';
    const BE = 'Belgium';
    const BZ = 'Belize';
    const BJ = 'Benin';
    const BM = 'Bermuda';
    const BT = 'Bhutan';
    const BO = 'Bolivia';
    const BA = 'Bosnia and Herzegovina';
    const BW = 'Botswana';
    const BV = 'Bouvet Island';
    const BR = 'Brazil';
    const IO = 'British Indian Ocean Territory';
    const BN = 'Brunei Darussalam';
    const BG = 'Bulgaria';
    const BF = 'Burkina Faso';
    const BI = 'Burundi';
    const KH = 'Cambodia';
    const CM = 'Cameroon';
    const CA = 'Canada';
    const CV = 'Cape Verde';
    const KY = 'Cayman Islands';
    const CF = 'Central African Republic';
    const TD = 'Chad';
    const CL = 'Chile';
    const CN = 'China';
    const CX = 'Christmas Island';
    const CC = 'Cocos (Keeling) Islands';
    const CO = 'Colombia';
    const KM = 'Comoros';
    const CG = 'Congo';
    const CD = 'Congo, the Democratic Republic of the';
    const CK = 'Cook Islands';
    const CR = 'Costa Rica';
    const CI = 'Cote D\'Ivoire';
    const HR = 'Croatia';
    const CU = 'Cuba';
    const CY = 'Cyprus';
    const CZ = 'Czech Republic';
    const DK = 'Denmark';
    const DJ = 'Djibouti';
    const DM = 'Dominica';
    const DO = 'Dominican Republic';
    const EC = 'Ecuador';
    const EG = 'Egypt';
    const SV = 'El Salvador';
    const GQ = 'Equatorial Guinea';
    const ER = 'Eritrea';
    const EE = 'Estonia';
    const ET = 'Ethiopia';
    const FK = 'Falkland Islands (Malvinas)';
    const FO = 'Faroe Islands';
    const FJ = 'Fiji';
    const FI = 'Finland';
    const FR = 'France';
    const GF = 'French Guiana';
    const PF = 'French Polynesia';
    const TF = 'French Southern Territories';
    const GA = 'Gabon';
    const GM = 'Gambia';
    const GE = 'Georgia';
    const DE = 'Germany';
    const GH = 'Ghana';
    const GI = 'Gibraltar';
    const GR = 'Greece';
    const GL = 'Greenland';
    const GD = 'Grenada';
    const GP = 'Guadeloupe';
    const GU = 'Guam';
    const GT = 'Guatemala';
    const GN = 'Guinea';
    const GW = 'Guinea-Bissau';
    const GY = 'Guyana';
    const HT = 'Haiti';
    const HM = 'Heard Island and Mcdonald Islands';
    const VA = 'Holy See (Vatican City State)';
    const HN = 'Honduras';
    const HK = 'Hong Kong';
    const HU = 'Hungary';
    const IS = 'Iceland';
    const IN = 'India';
    const ID = 'Indonesia';
    const IR = 'Iran, Islamic Republic of';
    const IQ = 'Iraq';
    const IE = 'Ireland';
    const IL = 'Israel';
    const IT = 'Italy';
    const JM = 'Jamaica';
    const JP = 'Japan';
    const JO = 'Jordan';
    const KZ = 'Kazakhstan';
    const KE = 'Kenya';
    const KI = 'Kiribati';
    const KP = 'Korea, Democratic People\'s Republic of';
    const KR = 'Korea, Republic of';
    const KW = 'Kuwait';
    const KG = 'Kyrgyzstan';
    const LA = 'Lao People\'s Democratic Republic';
    const LV = 'Latvia';
    const LB = 'Lebanon';
    const LS = 'Lesotho';
    const LR = 'Liberia';
    const LY = 'Libyan Arab Jamahiriya';
    const LI = 'Liechtenstein';
    const LT = 'Lithuania';
    const LU = 'Luxembourg';
    const MO = 'Macao';
    const MK = 'Macedonia, the Former Yugoslav Republic of';
    const MG = 'Madagascar';
    const MW = 'Malawi';
    const MY = 'Malaysia';
    const MV = 'Maldives';
    const ML = 'Mali';
    const MT = 'Malta';
    const MH = 'Marshall Islands';
    const MQ = 'Martinique';
    const MR = 'Mauritania';
    const MU = 'Mauritius';
    const YT = 'Mayotte';
    const MX = 'Mexico';
    const FM = 'Micronesia, Federated States of';
    const MD = 'Moldova, Republic of';
    const MC = 'Monaco';
    const MN = 'Mongolia';
    const MS = 'Montserrat';
    const MA = 'Morocco';
    const MZ = 'Mozambique';
    const MM = 'Myanmar';
    const NA = 'Namibia';
    const NR = 'Nauru';
    const NP = 'Nepal';
    const NL = 'Netherlands';
    const AN = 'Netherlands Antilles';
    const NC = 'New Caledonia';
    const NZ = 'New Zealand';
    const NI = 'Nicaragua';
    const NE = 'Niger';
    const NG = 'Nigeria';
    const NU = 'Niue';
    const NF = 'Norfolk Island';
    const MP = 'Northern Mariana Islands';
    const NO = 'Norway';
    const OM = 'Oman';
    const PK = 'Pakistan';
    const PW = 'Palau';
    const PS = 'Palestinian Territory, Occupied';
    const PA = 'Panama';
    const PG = 'Papua New Guinea';
    const PY = 'Paraguay';
    const PE = 'Peru';
    const PH = 'Philippines';
    const PN = 'Pitcairn';
    const PL = 'Poland';
    const PT = 'Portugal';
    const PR = 'Puerto Rico';
    const QA = 'Qatar';
    const RE = 'Reunion';
    const RO = 'Romania';
    const RU = 'Russian Federation';
    const RW = 'Rwanda';
    const SH = 'Saint Helena';
    const KN = 'Saint Kitts and Nevis';
    const LC = 'Saint Lucia';
    const PM = 'Saint Pierre and Miquelon';
    const VC = 'Saint Vincent and the Grenadines';
    const WS = 'Samoa';
    const SM = 'San Marino';
    const ST = 'Sao Tome and Principe';
    const SA = 'Saudi Arabia';
    const SN = 'Senegal';
    const CS = 'Serbia and Montenegro';
    const SC = 'Seychelles';
    const SL = 'Sierra Leone';
    const SG = 'Singapore';
    const SK = 'Slovakia';
    const SI = 'Slovenia';
    const SB = 'Solomon Islands';
    const SO = 'Somalia';
    const ZA = 'South Africa';
    const GS = 'South Georgia and the South Sandwich Islands';
    const ES = 'Spain';
    const LK = 'Sri Lanka';
    const SD = 'Sudan';
    const SR = 'Suriname';
    const SJ = 'Svalbard and Jan Mayen';
    const SZ = 'Swaziland';
    const SE = 'Sweden';
    const CH = 'Switzerland';
    const SY = 'Syrian Arab Republic';
    const TW = 'Taiwan, Province of China';
    const TJ = 'Tajikistan';
    const TZ = 'Tanzania, United Republic of';
    const TH = 'Thailand';
    const TL = 'Timor-Leste';
    const TG = 'Togo';
    const TK = 'Tokelau';
    const TO = 'Tonga';
    const TT = 'Trinidad and Tobago';
    const TN = 'Tunisia';
    const TR = 'Turkey';
    const TM = 'Turkmenistan';
    const TC = 'Turks and Caicos Islands';
    const TV = 'Tuvalu';
    const UG = 'Uganda';
    const UA = 'Ukraine';
    const AE = 'United Arab Emirates';
    const GB = 'United Kingdom';
    const US = 'United States';
    const UM = 'United States Minor Outlying Islands';
    const UY = 'Uruguay';
    const UZ = 'Uzbekistan';
    const VU = 'Vanuatu';
    const VE = 'Venezuela';
    const VN = 'Viet Nam';
    const VG = 'Virgin Islands, British';
    const VI = 'Virgin Islands, U.s.';
    const WF = 'Wallis and Futuna';
    const EH = 'Western Sahara';
    const YE = 'Yemen';
    const ZM = 'Zambia';
    const ZW = 'Zimbabwe';
}

# vim: set ts=4 sw=4 tw=80 noet :
