<?php /** @noinspection PhpUnusedAliasInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * HttpStatusCode
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Enums;

use Exen\Laravel\Enumeration\Enum;
use Exen\Laravel\Enumeration\Attributes\Description;

/**
 * HttpStatusCode Enum.
 *
 * @method static HttpStatusCode Continue()
 * @method static HttpStatusCode SwitchingProtocols()
 * @method static HttpStatusCode Processing()
 * @method static HttpStatusCode EarlyHints()
 * @method static HttpStatusCode OK()
 * @method static HttpStatusCode Created()
 * @method static HttpStatusCode Accepted()
 * @method static HttpStatusCode NonAuthoritativeInformation()
 * @method static HttpStatusCode NoContent()
 * @method static HttpStatusCode ResetContent()
 * @method static HttpStatusCode PartialContent()
 * @method static HttpStatusCode MultipleChoices()
 * @method static HttpStatusCode MovedPermanently()
 * @method static HttpStatusCode Found()
 * @method static HttpStatusCode SeeOther()
 * @method static HttpStatusCode NotModified()
 * @method static HttpStatusCode UseProxy()
 * @method static HttpStatusCode Unused()
 * @method static HttpStatusCode TemporaryRedirect()
 * @method static HttpStatusCode PermanentRedirect()
 * @method static HttpStatusCode BadRequest()
 * @method static HttpStatusCode Unauthorized()
 * @method static HttpStatusCode PaymentRequired()
 * @method static HttpStatusCode Forbidden()
 * @method static HttpStatusCode NotFound()
 * @method static HttpStatusCode MethodNotAllowed()
 * @method static HttpStatusCode NotAcceptable()
 * @method static HttpStatusCode ProxyAuthenticationRequired()
 * @method static HttpStatusCode RequestTimeout()
 * @method static HttpStatusCode Conflict()
 * @method static HttpStatusCode Gone()
 * @method static HttpStatusCode LengthRequired()
 * @method static HttpStatusCode PreconditionFailed()
 * @method static HttpStatusCode RequestEntityTooLarge()
 * @method static HttpStatusCode RequestURITooLong()
 * @method static HttpStatusCode UnsupportedMediaType()
 * @method static HttpStatusCode RequestedRangeNotSatisfiable()
 * @method static HttpStatusCode ExpectationFailed()
 * @method static HttpStatusCode ImATeapot()
 * @method static HttpStatusCode MisdirectedRequest()
 * @method static HttpStatusCode UnprocessableEntity()
 * @method static HttpStatusCode Locked()
 * @method static HttpStatusCode TooEarly()
 * @method static HttpStatusCode UpgradeRequired()
 * @method static HttpStatusCode PreconditionRequired()
 * @method static HttpStatusCode TooManyRequests()
 * @method static HttpStatusCode RequestHeaderFieldsTooLarge()
 * @method static HttpStatusCode UnavailableForLegalReasons()
 * @method static HttpStatusCode InternalServerError()
 * @method static HttpStatusCode NotImplemented()
 * @method static HttpStatusCode BadGateway()
 * @method static HttpStatusCode ServiceUnavailable()
 * @method static HttpStatusCode GatewayTimeout()
 * @method static HttpStatusCode HTTPVersionNotSupported()
 * @method static HttpStatusCode VariantAlsoNegotiates()
 * @method static HttpStatusCode InsufficientStorage()
 * @method static HttpStatusCode NetworkAuthenticationRequired()
 * @method static HttpStatusCode WebserverIsReturningAnUnknownError()
 * @method static HttpStatusCode ConnectionTimedOut()
 * @method static HttpStatusCode ATimeoutOccurred()
 *
 * @package Exen\Laravel\Enumeration\Enums
 */
#[Description('HttpStatusCode')]
final class HttpStatusCode extends Enum
{
    const Continue                           = 100;
    const SwitchingProtocols                 = 101;
    const Processing                         = 102;
    const EarlyHints                         = 103;
    const OK                                 = 200;
    const Created                            = 201;
    const Accepted                           = 202;
    const NonAuthoritativeInformation        = 203;
    const NoContent                          = 204;
    const ResetContent                       = 205;
    const PartialContent                     = 206;
    const MultipleChoices                    = 300;
    const MovedPermanently                   = 301;
    const Found                              = 302;
    const SeeOther                           = 303;
    const NotModified                        = 304;
    const UseProxy                           = 305;
    const Unused                             = 306;
    const TemporaryRedirect                  = 307;
    const PermanentRedirect                  = 308;
    const BadRequest                         = 400;
    const Unauthorized                       = 401;
    const PaymentRequired                    = 402;
    const Forbidden                          = 403;
    const NotFound                           = 404;
    const MethodNotAllowed                   = 405;
    const NotAcceptable                      = 406;
    const ProxyAuthenticationRequired        = 407;
    const RequestTimeout                     = 408;
    const Conflict                           = 409;
    const Gone                               = 410;
    const LengthRequired                     = 411;
    const PreconditionFailed                 = 412;
    const RequestEntityTooLarge              = 413;
    const RequestURITooLong                  = 414;
    const UnsupportedMediaType               = 415;
    const RequestedRangeNotSatisfiable       = 416;
    const ExpectationFailed                  = 417;
    const ImATeapot                          = 418;
    const MisdirectedRequest                 = 421;
    const UnprocessableEntity                = 422;
    const Locked                             = 423;
    const TooEarly                           = 425;
    const UpgradeRequired                    = 426;
    const PreconditionRequired               = 428;
    const TooManyRequests                    = 429;
    const RequestHeaderFieldsTooLarge        = 431;
    const UnavailableForLegalReasons         = 451;
    const InternalServerError                = 500;
    const NotImplemented                     = 501;
    const BadGateway                         = 502;
    const ServiceUnavailable                 = 503;
    const GatewayTimeout                     = 504;
    const HTTPVersionNotSupported            = 505;
    const VariantAlsoNegotiates              = 506;
    const InsufficientStorage                = 507;
    const NetworkAuthenticationRequired      = 511;
    const WebserverIsReturningAnUnknownError = 520;
    const ConnectionTimedOut                 = 522;
    const ATimeoutOccurred                   = 524;
}

# vim: set ts=4 sw=4 tw=80 noet :
