<?php /** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * FlaggedEnum
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration;

use Exen\Laravel\Enumeration\Exceptions\InvalidEnumMemberException;
use ReflectionException;
use function array_map;
use function array_reduce;
use function decbin;
use function is_array;
use function is_integer;

/**
 * Abstract FlaggedEnum Class.
 *
 * @method static static None()
 *
 * @package Exen\Laravel\Enumeration
 *
 * @phpstan-consistent-constructor
 */
abstract class FlaggedEnum extends Enum
{
    /** @var integer None */
    const None = 0x0;

    /**
     * Constant with default value for creating enum object.
     *
     * @var integer Default
     */
    const Default = self::None;

    /**
     * Construct a FlaggedEnum instance.
     *
     * @param integer[]|Enum[] $flags
     *
     * @throws ReflectionException
     * @throws InvalidEnumMemberException
     * @noinspection PhpUnusedLocalVariableInspection
     */
    public function __construct($flags = [])
    {
        $this->key = null;
        $this->description = null;
        $this->label = null;
        $this->ordinal = null;

        if (is_array($flags)) {
            $this->setFlags($flags);
        } else {
            try {
                parent::__construct($flags);
            } catch (InvalidEnumMemberException $ex) {
                $this->value = $flags;
            }
        }
    }

    /**
     * Set the flags for the enum to the given array of flags.
     *
     * @param integer[]|Enum[] $flags
     *
     * @return self
     * @throws InvalidEnumMemberException
     * @throws ReflectionException
     */
    public function setFlags(array $flags): self
    {
        $this->value = array_reduce($flags, function ($carry, $flag) {
            return $carry | static::fromValue($flag)->value;
        }, 0);

        return $this;
    }

    /**
     * Attempt to instantiate a new Enum using the given key or value.
     *
     * @param mixed $enumKeyOrValue
     *
     * @return static|null
     * @throws InvalidEnumMemberException
     * @throws ReflectionException
     */
    public static function coerce(mixed $enumKeyOrValue): ?Enum
    {
        if (is_integer($enumKeyOrValue)) {
            return static::fromValue($enumKeyOrValue);
        }

        return parent::coerce($enumKeyOrValue);
    }

    /**
     * Return a FlaggedEnum instance with defined flags.
     *
     * @param Enum[]|integer[] $flags
     *
     * @return self
     * @throws InvalidEnumMemberException
     * @throws ReflectionException
     */
    public static function flags(array $flags): self
    {
        return static::fromValue($flags);
    }

    /**
     * Add all flags to the enum.
     *
     * @return self
     * @throws InvalidEnumMemberException
     * @throws ReflectionException
     */
    public function addAllFlags(): self
    {
        return (new static)->addFlags(self::getValues());
    }

    /**
     * Add the given flags to the enum.
     *
     * @param integer[]|Enum[] $flags
     *
     * @return self
     * @throws InvalidEnumMemberException
     * @throws ReflectionException
     */
    public function addFlags(array $flags): self
    {
        array_map(function ($flag) {
            $this->addFlag($flag);
        }, $flags);

        return $this;
    }

    /**
     * Add the given flag to the enum.
     *
     * @param integer|Enum $flag
     *
     * @return self
     * @throws InvalidEnumMemberException
     * @throws ReflectionException
     */
    public function addFlag(Enum|int $flag): self
    {
        $this->value |= static::fromValue($flag)->value;

        return $this;
    }

    /**
     * Remove the given flags from the enum.
     *
     * @param integer[]|Enum[] $flags
     *
     * @return self
     * @throws InvalidEnumMemberException
     * @throws ReflectionException
     */
    public function removeFlags(array $flags): self
    {
        array_map(function ($flag) {
            $this->removeFlag($flag);
        }, $flags);

        return $this;
    }

    /**
     * Remove the given flag from the enum.
     *
     * @param integer|Enum|array $flag
     *
     * @return self
     * @throws InvalidEnumMemberException
     * @throws ReflectionException
     */
    public function removeFlag(Enum|int|array $flag): self
    {
        $this->value &= ~static::fromValue($flag)->value;

        return $this;
    }

    /**
     * Remove all flags from the enum.
     *
     * @return self
     */
    public function removeAllFlags(): self
    {
        return static::None();
    }

    /**
     * Check if the enum has all the specified flags.
     *
     * @param integer[]|Enum[] $flags
     *
     * @return boolean
     * @throws InvalidEnumMemberException
     * @throws ReflectionException
     */
    public function hasFlags(array $flags): bool
    {
        foreach ($flags as $flag) {
            if (!static::hasFlag($flag)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Check if the enum has the specified flag.
     *
     * @param Enum|integer $flag
     *
     * @return boolean
     * @throws InvalidEnumMemberException
     * @throws ReflectionException
     */
    public function hasFlag(Enum|int $flag): bool
    {
        $flagValue = static::fromValue($flag)->value;

        if ($flagValue === 0) {
            return false;
        }

        return ($flagValue & $this->value) === $flagValue;
    }

    /**
     * Check if the enum doesn't have any of the specified flags.
     *
     * @param integer[]|Enum[] $flags
     *
     * @return boolean
     * @throws InvalidEnumMemberException
     * @throws ReflectionException
     */
    public function notHasFlags(array $flags): bool
    {
        foreach ($flags as $flag) {
            if (!static::notHasFlag($flag)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Check if the enum does not have the specified flag.
     *
     * @param integer|Enum $flag
     *
     * @return boolean
     * @throws InvalidEnumMemberException
     * @throws ReflectionException
     */
    public function notHasFlag(Enum|int $flag): bool
    {
        return !$this->hasFlag($flag);
    }

    /**
     * Return the flags as an array of instances.
     *
     * @return Enum[]
     * @throws InvalidEnumMemberException
     * @throws ReflectionException
     */
    public function getFlags(): array
    {
        $members = static::getInstances();
        $flags = [];

        foreach ($members as $member) {
            if ($this->hasFlag($member)) {
                $flags[] = $member;
            }
        }

        return $flags;
    }

    /**
     * Check if there are multiple flags set on the enum.
     *
     * @return boolean
     */
    public function hasMultipleFlags(): bool
    {
        return ($this->value & ($this->value - 1)) !== 0;
    }

    /**
     * Get the bitmask for the enum.
     *
     * @return int
     */
    public function getBitmask(): int
    {
        return (int)decbin($this->value);
    }
}

# vim: set ts=4 sw=4 tw=80 noet :
