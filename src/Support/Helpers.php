<?php /** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * Helpers
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

/**
 * Helpers.
 *
 * @package Exen\Laravel\Enumeration\Support
 */


# vim: set ts=4 sw=4 tw=80 noet :
