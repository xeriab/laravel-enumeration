<?php /** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * UndefinedEnumMemberException
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Exceptions;

use Exception;
use function sprintf;
use function var_export;

/**
 * UndefinedEnumMemberException Class.
 *
 * @package Exen\Laravel\Enumeration\Exceptions
 */
final class UndefinedEnumMemberException extends AbstractUndefinedEnumMemberException
{
    /**
     * Construct a new Undefined Enum Member Exception.
     *
     * @param string $className The name of the class from which the member was requested.
     * @param string $property The name of the property used to search for the member.
     * @param mixed $value The value of the property used to search for the member.
     * @param Exception|null $cause The cause, if available.
     */
    public function __construct(string $className, string $property, mixed $value, ?Exception $cause = null)
    {
        parent::__construct(
            sprintf(
                'No member with %s equal to %s defined in class %s.',
                $property,
                var_export($value, true),
                var_export($className, true)
            ),
            $className,
            $property,
            $value,
            $cause
        );
    }
}

# vim: set ts=4 sw=4 tw=80 noet :

