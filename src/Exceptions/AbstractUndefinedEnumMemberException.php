<?php /** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * UndefinedEnumMemberException
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Exceptions;

use Exception;

/**
 * AbstractUndefinedEnumMemberException Class.
 *
 * @abstract
 *
 * @package Exen\Laravel\Enumeration\Exceptions
 */
abstract class AbstractUndefinedEnumMemberException extends Exception
{
    /** @var string $className */
    protected string $className;

    /** @var string $property */
    protected string $property;

    /** @var mixed $value */
    protected mixed $value;

    /**
     * Construct a new Undefined Enum Member Exception.
     *
     * @param string $message The exception message.
     * @param string $className The name of the class from which the member was requested.
     * @param string $property The name of the property used to search for the member.
     * @param mixed $value The value of the property used to search for the member.
     * @param Exception|null $cause The cause, if available.
     */
    public function __construct(
        string $message,
        string $className,
        string $property,
        mixed $value,
        ?Exception $cause = null
    ) {
        $this->className = $className;
        $this->property = $property;
        $this->value = $value;

        parent::__construct($message, 0, $cause);
    }

    /**
     * Get the class name.
     *
     * @return string The class name.
     */
    public function getClassName(): string
    {
        return $this->className;
    }

    /**
     * Get the property name.
     *
     * @return string The property name.
     */
    public function getProperty(): string
    {
        return $this->property;
    }

    /**
     * Get the value of the property used to search for the member.
     *
     * @return mixed The value.
     */
    public function getValue(): mixed
    {
        return $this->value;
    }
}

# vim: set ts=4 sw=4 tw=80 noet :

