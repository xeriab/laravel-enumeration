<?php /** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * UnexpectedEnumValueException
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Exceptions;

use Exen\Laravel\Enumeration\Enum;
use ReflectionException;
use RuntimeException;
use function class_basename;
use function implode;

/**
 * UnexpectedEnumValueException Class.
 *
 * @package Exen\Laravel\Enumeration\Exceptions
 */
class UnexpectedEnumValueException extends RuntimeException
{
    /**
     * Create an UnexpectedEnumValueException.
     *
     * @param mixed $invalidValue
     * @param Enum $enum
     *
     * @return void
     * @throws ReflectionException
     */
    public function __construct($invalidValue, Enum $enum)
    {
        $enumValues = implode(', ', $enum::getValues());
        $enumClassName = class_basename($enum);

        parent::__construct("The given value `$invalidValue` is not in the Enum `$enumClassName`. Possible values are [$enumValues].");
    }
}

# vim: set ts=4 sw=4 tw=80 noet :

