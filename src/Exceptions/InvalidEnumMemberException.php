<?php /** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * InvalidEnumMemberException
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Exceptions;

use Exception;
use Exen\Laravel\Enumeration\Enum;
use ReflectionException;
use function class_basename;
use function gettype;
use function implode;

/**
 * InvalidEnumMemberException Class.
 *
 * @package Exen\Laravel\Enumeration\Exceptions
 */
class InvalidEnumMemberException extends Exception
{
    /**
     * Create an InvalidEnumMemberException.
     *
     * @param mixed $invalidValue
     * @param Enum $enum
     *
     * @return void
     * @throws ReflectionException
     */
    public function __construct($invalidValue, Enum $enum)
    {
        $invalidValueType = gettype($invalidValue);
        $enumValues = implode(', ', $enum::getValues());
        $enumClassName = class_basename($enum);

        parent::__construct("Cannot construct an instance of $enumClassName using the value ($invalidValueType) `$invalidValue`. Possible values are [$enumValues].");
    }
}

# vim: set ts=4 sw=4 tw=80 noet :

