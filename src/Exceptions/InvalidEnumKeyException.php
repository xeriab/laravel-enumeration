<?php /** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * InvalidEnumKeyException
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Exceptions;

use Exception;
use function class_basename;
use function gettype;
use function implode;

/**
 * InvalidEnumKeyException Class.
 *
 * @package Exen\Laravel\Enumeration\Exceptions
 */
class InvalidEnumKeyException extends Exception
{
    /**
     * Create an InvalidEnumKeyException.
     *
     * @param mixed $invalidKey
     * @param string $enumClass A class-string of type \Exen\Laravel\Enumeration\Enum
     *
     * @return void
     */
    public function __construct($invalidKey, string $enumClass)
    {
        $invalidValueType = gettype($invalidKey);
        $enumKeys = implode(', ', $enumClass::getKeys());
        $enumClassName = class_basename($enumClass);

        parent::__construct("Cannot construct an instance of $enumClassName using the key ($invalidValueType) `$invalidKey`. Possible keys are [$enumKeys].");
    }
}

# vim: set ts=4 sw=4 tw=80 noet :
