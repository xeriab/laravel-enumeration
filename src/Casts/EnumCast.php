<?php /** @noinspection PhpUndefinedMethodInspection */
/** @noinspection SpellCheckingInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * EnumCast
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Casts;

use Exen\Laravel\Enumeration\Enum;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

/**
 * EnumCast Class.
 *
 * @package Exen\Laravel\Enumeration\Casts
 */
class EnumCast implements CastsAttributes
{
    /**
     * The name of the Enumeration class.
     *
     * @var string $enumClass
     */
    protected string $enumClass;

    public function __construct(string $enumClass)
    {
        $this->enumClass = $enumClass;
    }

    public function get($model, string $key, $value, array $attributes)
    {
        return $this->castEnum($value);
    }

    /**
     * @param mixed $value
     *
     * @return Enum|null
     */
    protected function castEnum(mixed $value): ?Enum
    {
        if ($value === null or $value instanceof $this->enumClass) {
            /** @noinspection PhpExpressionAlwaysNullInspection */
            return $value;
        }

        $value = $this->getCastableValue($value);

        if ($value === null) {
            return null;
        }

        return $this->enumClass::fromValue($value);
    }

    /**
     * Retrieve the value that can be casted into Enum
     *
     * @param mixed $value
     *
     * @return mixed
     */
    protected function getCastableValue(mixed $value): mixed
    {
        // If the enum has overridden the `parseDatabase` method, use it to get the cast value
        $value = $this->enumClass::parseDatabase($value);

        if ($value === null) {
            return null;
        }

        // If the value exists in the enum (using strict type checking) return it
        if ($this->enumClass::hasValue($value)) {
            return $value;
        }

        // Find the value in the enum that the incoming value can be coerced to
        foreach ($this->enumClass::getValues() as $enumValue) {
            if ($value == $enumValue) {
                return $enumValue;
            }
        }

        // Fall back to trying to construct it directly (will result in an error since it doesn't exist)
        return $value;
    }

    public function set($model, string $key, $value, array $attributes): array
    {
        $value = $this->castEnum($value);

        return [$key => $this->enumClass::serializeDatabase($value)];
    }
}

# vim: set ts=4 sw=4 tw=80 noet :
