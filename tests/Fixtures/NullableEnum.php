<?php /** @noinspection PhpUnnecessaryCurlyVarSyntaxInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * NullableEnum
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Tests\Fixtures;

use Exen\Laravel\Enumeration\Enum;

/**
 * NullableEnum Class.
 *
 * @method static NullableEnum None()
 * @method static NullableEnum Initialized()
 * @method static NullableEnum Completed()
 *
 * @package Exen\Laravel\Enumeration\Tests\Fixtures
 */
final class NullableEnum extends Enum
{
    const None        = null;
    const Initialized = 0x1;
    const Completed   = 0x2;
}

# vim: set ts=4 sw=4 tw=80 noet :
