<?php /** @noinspection PhpUnnecessaryCurlyVarSyntaxInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * EnumTypeHintTest
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Tests;

use PHPUnit\Framework\TestCase;
use Exen\Laravel\Enumeration\Tests\Enums\UserType;

/**
 * EnumTypeHintTest Class.
 *
 * @package Exen\Laravel\Enumeration\Tests
 */
class EnumTypeHintTest extends TestCase
{
    public function test_can_pass_an_enum_instance_to_a_type_hinted_method()
    {
        $userType1 = UserType::fromValue(UserType::SuperAdministrator);
        $userType2 = UserType::fromValue(UserType::Moderator);

        $this->assertTrue($this->typeHintedMethod($userType1));
        $this->assertFalse($this->typeHintedMethod($userType2));
    }

    private function typeHintedMethod(UserType $userType)
    {
        if ($userType->is(UserType::SuperAdministrator)) {
            return true;
        }

        return false;
    }
}

# vim: set ts=4 sw=4 tw=80 noet :
