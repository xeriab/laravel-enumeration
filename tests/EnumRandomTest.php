<?php /** @noinspection PhpUnnecessaryCurlyVarSyntaxInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * EnumRandomTest
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Tests;

use Exen\Laravel\Enumeration\Tests\Enums\SingleValue;
use PHPUnit\Framework\TestCase;

/**
 * EnumRandomTest Class.
 *
 * @package Exen\Laravel\Enumeration\Tests
 */
class EnumRandomTest extends TestCase
{
    public function test_can_get_random_key()
    {
        $key = SingleValue::getRandomKey();

        $this->assertSame(
            SingleValue::getKey(SingleValue::KEY),
            $key
        );
    }

    public function test_can_get_random_value()
    {
        $value = SingleValue::getRandomValue();

        $this->assertSame(SingleValue::KEY, $value);
    }

    public function test_can_get_random_instance()
    {
        $instance = SingleValue::getRandomInstance();

        $this->assertTrue(
            $instance->is(SingleValue::KEY)
        );
    }
}

# vim: set ts=4 sw=4 tw=80 noet :
