<?php /** @noinspection PhpUnnecessaryCurlyVarSyntaxInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * NativeCastModel
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Tests\Models;

use Exen\Laravel\Enumeration\Tests\Enums\UserType;
use Exen\Laravel\Enumeration\Tests\Enums\UserTypeCustomCast;
use Illuminate\Database\Eloquent\Model;

/**
 * NativeCastModel Class.
 *
 * Description should be kept
 *
 * @property UserType|null $user_type
 * @property UserTypeCustomCast|null $user_type_custom
 *
 * @package Exen\Laravel\Enumeration\Tests\Models
 */
class NativeCastModel extends Model
{
    protected $casts = [
        'user_type'        => UserType::class,
        'user_type_custom' => UserTypeCustomCast::class,
    ];

    protected $fillable = [
        'user_type',
        'user_type_custom',
    ];
}

# vim: set ts=4 sw=4 tw=80 noet :
