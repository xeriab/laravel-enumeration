<?php /** @noinspection PhpUnnecessaryCurlyVarSyntaxInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * WithQueriesFlaggedEnums
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Tests\Models;

use Exen\Laravel\Enumeration\Traits\QueriesFlaggedEnums;
use Illuminate\Database\Eloquent\Model;

/**
 * WithQueriesFlaggedEnums Class.
 *
 * Description should be kept
 *
 * @package Exen\Laravel\Enumeration\Tests\Models
 */
class WithQueriesFlaggedEnums extends Model
{
    use QueriesFlaggedEnums;

    public $table = 'test_models';

    protected $guarded = [];
}

# vim: set ts=4 sw=4 tw=80 noet :
