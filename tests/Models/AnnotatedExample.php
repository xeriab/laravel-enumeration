<?php /** @noinspection PhpUnnecessaryCurlyVarSyntaxInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * AnnotatedExample
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Tests\Models;

use Exen\Laravel\Enumeration\Tests\Enums\UserType;
use Illuminate\Database\Eloquent\Model;

/**
 * AnnotatedExample Class.
 *
 * Description should be kept
 *
 * @property UserType|null $user_type
 *
 * @package Exen\Laravel\Enumeration\Tests\Models
 */
class AnnotatedExample extends Model
{
    protected $casts = [
        'user_type' => UserType::class,
    ];

    protected $fillable = [
        'user_type',
    ];
}

# vim: set ts=4 sw=4 tw=80 noet :
