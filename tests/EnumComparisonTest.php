<?php /** @noinspection PhpUnnecessaryCurlyVarSyntaxInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * EnumComparisonTest
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Tests;

use ArrayIterator;
use Exen\Laravel\Enumeration\Tests\Enums\IntegerValues;
use Exen\Laravel\Enumeration\Tests\Enums\StringValues;
use Exen\Laravel\Enumeration\Tests\Enums\UserType;
use PHPUnit\Framework\TestCase;

/**
 * EnumComparisonTest Class.
 *
 * @package Exen\Laravel\Enumeration\Tests
 */
class EnumComparisonTest extends TestCase
{
    public function test_comparison_against_plain_value_matching()
    {
        $admin = UserType::fromValue(UserType::Administrator);

        $this->assertTrue($admin->is(UserType::Administrator));
    }

    public function test_comparison_against_plain_value_not_matching()
    {
        $admin = UserType::fromValue(UserType::Administrator);

        $this->assertFalse($admin->is(UserType::SuperAdministrator));
        $this->assertFalse($admin->is('some-random-value'));
        $this->assertTrue($admin->isNot(UserType::SuperAdministrator));
        $this->assertTrue($admin->isNot('some-random-value'));
    }

    public function test_comparison_against_itself_matches()
    {
        $admin = UserType::fromValue(UserType::Administrator);

        $this->assertTrue($admin->is($admin));
    }

    public function test_comparison_against_other_instances_matches()
    {
        $admin = UserType::fromValue(UserType::Administrator);
        $anotherAdmin = UserType::fromValue(UserType::Administrator);

        $this->assertTrue($admin->is($anotherAdmin));
    }

    public function test_comparison_against_other_instances_not_matching()
    {
        $admin = UserType::fromValue(UserType::Administrator);
        $superAdmin = UserType::fromValue(UserType::SuperAdministrator);

        $this->assertFalse($admin->is($superAdmin));
    }

    public function test_enum_instance_in_array()
    {
        $administrator = new StringValues(StringValues::Administrator);

        $this->assertTrue($administrator->in([
            StringValues::Moderator,
            StringValues::Administrator
        ]));
        $this->assertTrue($administrator->in([
            new StringValues(StringValues::Moderator),
            new StringValues(StringValues::Administrator)
        ]));
        $this->assertTrue($administrator->in([StringValues::Administrator]));
        $this->assertFalse($administrator->in([StringValues::Moderator]));
    }

    public function test_enum_instance_in_iterator()
    {
        $administrator = new StringValues(StringValues::Administrator);

        $this->assertTrue($administrator->in(new ArrayIterator([
            StringValues::Moderator,
            StringValues::Administrator
        ])));
        $this->assertTrue($administrator->in(new ArrayIterator([
            new StringValues(StringValues::Moderator),
            new StringValues(StringValues::Administrator)
        ])));
        $this->assertTrue($administrator->in(new ArrayIterator([StringValues::Administrator])));
        $this->assertFalse($administrator->in(new ArrayIterator([StringValues::Moderator])));
    }

    public function test_enum_instance_not_in_array()
    {
        $administrator = new StringValues(StringValues::Administrator);

        $this->assertFalse($administrator->notIn([
            StringValues::Moderator,
            StringValues::Administrator
        ]));
        $this->assertFalse($administrator->notIn([
            new StringValues(StringValues::Moderator),
            new StringValues(StringValues::Administrator)
        ]));
        $this->assertFalse($administrator->notIn([StringValues::Administrator]));
        $this->assertTrue($administrator->notIn([StringValues::Moderator]));
    }

    public function test_enum_instance_not_in_iterator()
    {
        $administrator = new StringValues(StringValues::Administrator);

        $this->assertFalse($administrator->notIn(new ArrayIterator([
            StringValues::Moderator,
            StringValues::Administrator
        ])));
        $this->assertFalse($administrator->notIn(new ArrayIterator([
            new StringValues(StringValues::Moderator),
            new StringValues(StringValues::Administrator)
        ])));
        $this->assertFalse($administrator->notIn(new ArrayIterator([StringValues::Administrator])));
        $this->assertTrue($administrator->notIn(new ArrayIterator([StringValues::Moderator])));
    }

    /**
     * @test
     * Verify that relational comparison of Enum object uses attribute `$value`
     *
     * "comparison operation stops and returns to the first unequal property found."
     * as stated in https://www.php.net/manual/en/language.oop5.object-comparison.php#98725
     *
     * @return void
     */
    public function test_object_relational_comparison(): void
    {
        $a = IntegerValues::A();
        $b = IntegerValues::B();

        $this->assertTrue($a > $b);
    }
}

# vim: set ts=4 sw=4 tw=80 noet :
