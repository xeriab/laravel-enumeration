<?php /** @noinspection PhpUnnecessaryCurlyVarSyntaxInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * EnumKeyTest
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Tests;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Exen\Laravel\Enumeration\Rules\EnumKey;
use Exen\Laravel\Enumeration\Tests\Enums\UserType;
use Exen\Laravel\Enumeration\Tests\Enums\StringValues;

/**
 * EnumKeyTest Class.
 *
 * @package Exen\Laravel\Enumeration\Tests
 */
class EnumKeyTest extends TestCase
{
    public function test_validation_passes()
    {
        $passes1 = (new EnumKey(UserType::class))->passes('', 'Administrator');
        $passes2 = (new EnumKey(StringValues::class))->passes('', 'Administrator');
        $passes3 = (new EnumKey(StringValues::class))->passes('', 'administrator');

        $this->assertTrue($passes1);
        $this->assertTrue($passes2);
        $this->assertFalse($passes3);
    }

    public function test_validation_fails()
    {
        $fails1 = (new EnumKey(UserType::class))->passes('', 'Anything else');
        $fails2 = (new EnumKey(UserType::class))->passes('', 2);
        $fails3 = (new EnumKey(UserType::class))->passes('', '2');

        $this->assertFalse($fails1);
        $this->assertFalse($fails2);
        $this->assertFalse($fails3);
    }

    public function test_an_exception_is_thrown_if_an_non_existing_class_is_passed()
    {
        $this->expectException(InvalidArgumentException::class);

        (new EnumKey('PathToAClassThatDoesntExist'))->passes('', 'Test');
    }

    public function test_can_serialize_to_string()
    {
        $rule = new EnumKey(UserType::class);

        $this->assertSame('enum_key:' . UserType::class, (string) $rule);
    }
}

# vim: set ts=4 sw=4 tw=80 noet :
