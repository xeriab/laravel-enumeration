<?php /** @noinspection PhpUnnecessaryCurlyVarSyntaxInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * StringValues
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Tests\Enums;

use Exen\Laravel\Enumeration\Enum;

/**
 * StringValues Class.
 *
 * @method static StringValues Administrator()
 * @method static StringValues Moderator()
 *
 * @package Exen\Laravel\Enumeration\Tests\Enums
 */
final class StringValues extends Enum
{
    const Administrator = 'administrator';
    const Moderator = 'moderator';
}

# vim: set ts=4 sw=4 tw=80 noet :
