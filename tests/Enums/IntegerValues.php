<?php /** @noinspection PhpUnnecessaryCurlyVarSyntaxInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * IntegerValues
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Tests\Enums;

use Exen\Laravel\Enumeration\Enum;

/**
 * IntegerValues Class.
 *
 * @method static static B()
 * @method static static A()
 *
 * @package Exen\Laravel\Enumeration\Tests\Enums
 */
final class IntegerValues extends Enum
{
    const B = 1;
    const A = 2;
}

# vim: set ts=4 sw=4 tw=80 noet :
