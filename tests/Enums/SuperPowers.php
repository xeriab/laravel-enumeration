<?php /** @noinspection PhpUnnecessaryCurlyVarSyntaxInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * SuperPowers
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Tests\Enums;

use Exen\Laravel\Enumeration\FlaggedEnum;

/**
 * SuperPowers Class.
 *
 * @method static SuperPowers Flight()
 * @method static SuperPowers Invisibility()
 * @method static SuperPowers LaserVision()
 * @method static SuperPowers Strength()
 * @method static SuperPowers Teleportation()
 * @method static SuperPowers Immortality()
 * @method static SuperPowers TimeTravel()
 * @method static SuperPowers Superman()
 *
 * @package Exen\Laravel\Enumeration\Tests\Enums
 */
final class SuperPowers extends FlaggedEnum
{
    const Flight        = 1 << 0;
    const Invisibility  = 1 << 1;
    const LaserVision   = 1 << 2;
    const Strength      = 1 << 3;
    const Teleportation = 1 << 4;
    const Immortality   = 1 << 5;
    const TimeTravel    = 1 << 6;

    const Superman = self::Flight | self::Strength | self::LaserVision;
}

# vim: set ts=4 sw=4 tw=80 noet :
