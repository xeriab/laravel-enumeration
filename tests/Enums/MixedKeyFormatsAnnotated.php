<?php /** @noinspection PhpUnnecessaryCurlyVarSyntaxInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * MixedKeyFormatsAnnotated
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Tests\Enums;

use Exen\Laravel\Enumeration\Enum;

/**
 * MixedKeyFormatsAnnotated Class.
 *
 * @method static static Normal()
 * @method static static MultiWordKeyName()
 * @method static static UPPERCASE()
 * @method static static UPPERCASE_SNAKE_CASE()
 * @method static static lowercase_snake_case()
 * @method static static Default()
 *
 * @package Exen\Laravel\Enumeration\Tests\Enums
 */
final class MixedKeyFormatsAnnotated extends Enum
{
    const Normal               = 1;
    const MultiWordKeyName     = 2;
    const UPPERCASE            = 3;
    const UPPERCASE_SNAKE_CASE = 4;
    const lowercase_snake_case = 5;
}
