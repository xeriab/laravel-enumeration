<?php /** @noinspection PhpUnnecessaryCurlyVarSyntaxInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * UserTypeCustomCast
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Tests\Enums;

use Exen\Laravel\Enumeration\Enum;

/**
 * UserTypeCustomCast Class.
 *
 * @method static UserTypeCustomCast Administrator()
 * @method static UserTypeCustomCast Moderator()
 * @method static UserTypeCustomCast Subscriber()
 * @method static UserTypeCustomCast SuperAdministrator()
 *
 * @package Exen\Laravel\Enumeration\Tests\Enums
 */
final class UserTypeCustomCast extends Enum
{
    const Administrator      = 0;
    const Moderator          = 1;
    const Subscriber         = 2;
    const SuperAdministrator = 3;

    public static function parseDatabase(mixed $value): mixed
    {
        if (! $value) {
            return null;
        }

        return explode('-', $value)[1] ?? null;
    }

    public static function serializeDatabase(mixed $value): mixed
    {
        if (! $value) {
            return null;
        }

        return 'type-' . $value;
    }
}

# vim: set ts=4 sw=4 tw=80 noet :
