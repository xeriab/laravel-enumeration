<?php /** @noinspection PhpUnnecessaryCurlyVarSyntaxInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * MixedKeyFormats
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Tests\Enums;

use Exen\Laravel\Enumeration\Enum;

/**
 * MixedKeyFormats Class.
 *
 * @package Exen\Laravel\Enumeration\Tests\Enums
 */
final class MixedKeyFormats extends Enum
{
    const Normal = 1;
    const MultiWordKeyName = 2;
    const UPPERCASE = 3;
    const UPPERCASE_SNAKE_CASE = 4;
    const lowercase_snake_case = 5;
    const UPPERCASE_SNAKE_CASE_NUMERIC_SUFFIX_2 = 6;
    const lowercase_snake_case_numeric_suffix_2 = 7;
}

# vim: set ts=4 sw=4 tw=80 noet :
