<?php /** @noinspection PhpUnnecessaryCurlyVarSyntaxInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * AnnotateTestTwoEnum
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Tests\Enums\Annotate;

use Exen\Laravel\Enumeration\Enum;

/**
 * AnnotateTestTwoEnum Class.
 *
 * @method static static TEST()
 * @method static static XYZ()
 *
 * @package Exen\Laravel\Enumeration\Tests\Enums\Annotate
 */
final class AnnotateTestTwoEnum extends Enum
{
    const TEST = 'test';
    const XYZ = 'xyz';
}

# vim: set ts=4 sw=4 tw=80 noet :
