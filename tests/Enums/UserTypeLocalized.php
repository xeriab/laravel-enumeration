<?php /** @noinspection PhpUnnecessaryCurlyVarSyntaxInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * UserTypeLocalized
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Tests\Enums;

use Exen\Laravel\Enumeration\Enum;
use Exen\Laravel\Enumeration\Contracts\LocalizedEnum;

/**
 * UserTypeLocalized Class.
 *
 * @package Exen\Laravel\Enumeration\Tests\Enums
 */
final class UserTypeLocalized extends Enum implements LocalizedEnum
{
    const Moderator = 0;
    const Administrator = 1;
    const SuperAdministrator = 2;
}

# vim: set ts=4 sw=4 tw=80 noet :
