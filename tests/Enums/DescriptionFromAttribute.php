<?php /** @noinspection PhpAttributeIsNotRepeatableInspection */
/** @noinspection PhpUnnecessaryCurlyVarSyntaxInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * DescriptionFromAttribute
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Tests\Enums;

use Exen\Laravel\Enumeration\Enum;
use Exen\Laravel\Enumeration\Attributes\Description;

/**
 * DescriptionFromAttribute Class.
 *
 * @method static static Administrator()
 * @method static static Moderator()
 * @method static static Subscriber()
 * @method static static SuperAdministrator()
 * @method static static InvalidCaseWithMultipleDescriptions()
 *
 * @package Exen\Laravel\Enumeration\Tests\Enums
 */
final class DescriptionFromAttribute extends Enum
{
    #[Description('Admin')]
    const Administrator = 0;

    #[Description('Mod (Level 1)')]
    const Moderator = 1;

    const Subscriber = 2;

    const SuperAdministrator = 3;

    #[Description('First description')]
    #[Description('Second description')]
    const InvalidCaseWithMultipleDescriptions = 4;

    /**
     * {@inheritDoc}
     */
    public static function getDescription(mixed $value): string
    {
        if ($value === self::SuperAdministrator) {
            return 'Super Admin';
        }

        return parent::getDescription($value);
    }
}

# vim: set ts=4 sw=4 tw=80 noet :
