<?php /** @noinspection PhpUnnecessaryCurlyVarSyntaxInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * AnnotatedConstants
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Tests\Enums;

use Exen\Laravel\Enumeration\Enum;

/**
 * AnnotatedConstants Class.
 *
 * @package Exen\Laravel\Enumeration\Tests\Enums
 */
final class AnnotatedConstants extends Enum
{
    /**
     * Internal and deprecated
     *
     * @internal
     *
     * @deprecated 1.0 Deprecation description
     */
    const InternalDeprecated = 0;
    /**
     * Internal
     *
     * @internal
     */
    const Internal = 1;
    /**
     * Deprecated
     *
     * @deprecated
     */
    const Deprecated = 2;
    const Unannotated = 3;
}

# vim: set ts=4 sw=4 tw=80 noet :
