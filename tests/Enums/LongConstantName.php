<?php /** @noinspection PhpUnnecessaryCurlyVarSyntaxInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * LongConstantName
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Tests\Enums;

use Exen\Laravel\Enumeration\Enum;

/**
 * LongConstantName Class.
 *
 * @method static static A_VERY_LONG_CONSTANT_NAME_THAT_WOULD_DEFINITELY_BE_WRAPPED_IF_WRAPPING_WASNT_DISABLED_BECAUSE_IT_EXCEEDS_AT_LEAST_120_CHARACTERS()
 *
 * @package Exen\Laravel\Enumeration\Tests\Enums
 */
final class LongConstantName extends Enum
{
    const A_VERY_LONG_CONSTANT_NAME_THAT_WOULD_DEFINITELY_BE_WRAPPED_IF_WRAPPING_WASNT_DISABLED_BECAUSE_IT_EXCEEDS_AT_LEAST_120_CHARACTERS = 1;
}

# vim: set ts=4 sw=4 tw=80 noet :
