<?php /** @noinspection PhpUnnecessaryCurlyVarSyntaxInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * BaseEnumTest
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Tests;

use PHPUnit\Framework\TestCase;

/**
 * Abstract BaseEnumTest Class.
 *
 * @abstract
 *
 * @package Exen\Laravel\Enumeration\Tests
 */
abstract class BaseEnumTest extends TestCase
{
}

# vim: set ts=4 sw=4 tw=80 noet :
