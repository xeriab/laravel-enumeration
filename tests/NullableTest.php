<?php /** @noinspection PhpUnnecessaryCurlyVarSyntaxInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * NullableTest
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Tests;

use Exen\Laravel\Enumeration\Enum;
use Exen\Laravel\Enumeration\Tests\Fixtures\NullableEnum;
use ReflectionException;

/**
 * NullableTest Class.
 *
 * @package Exen\Laravel\Enumeration\Tests
 */
class NullableTest extends BaseEnumTest
{
    /**
     * @test
     * @throws ReflectionException
     */
    public function can_be_null_if_a_null_value_is_explicitly_present()
    {
        $unknown = NullableEnum::create();

        $this->assertInstanceOf(Enum::class, $unknown);
        $this->assertNull($unknown->value);
    }

    /**
     * @test
     * @throws ReflectionException
     */
    public function can_be_created_with_create_method_with_null_as_value_parameter()
    {
        $unknown = NullableEnum::create();

        $this->assertInstanceOf(Enum::class, $unknown);
        $this->assertNull($unknown->value);
    }

    /**
     * @test
     */
    public function can_be_created_with_constructor_without_parameter()
    {
        $unknown = new NullableEnum();

        $this->assertInstanceOf(Enum::class, $unknown);
        $this->assertNull($unknown->value);
    }

    /**
     * @test
     */
    public function can_be_created_with_constructor_with_null_as_parameter_value()
    {
        $unknown = new NullableEnum(null);

        $this->assertInstanceOf(Enum::class, $unknown);
        $this->assertNull($unknown->value);
    }

    /**
     * @test
     */
    public function this_case_is_practically_the_same_as_the_one_above()
    {
        $unknown = new NullableEnum(NullableEnum::None);

        $this->assertInstanceOf(Enum::class, $unknown);
        $this->assertNull($unknown->value);
    }

    /**
     * @test
     */
    public function and_of_course_magic_constructor_works_as_well()
    {
        $unknown = NullableEnum::None();

        $this->assertInstanceOf(Enum::class, $unknown);
        $this->assertNull($unknown->value);
    }

    /**
     * @test
     */
    public function it_properly_returns_the_values()
    {
        $values = NullableEnum::values();

        $this->assertCount(3, $values);

        $this->assertNull($values[0]);
        $this->assertEquals(NullableEnum::Initialized, $values[1]);
        $this->assertEquals(NullableEnum::Completed, $values[2]);
    }

    /**
     * @test
     * @throws ReflectionException
     */
    public function it_properly_returns_the_choices()
    {
        $choices = NullableEnum::choices();

        $this->assertCount(3, $choices);

        $this->assertArrayHasKey('', $choices);
        $this->assertArrayHasKey(1, $choices);
        $this->assertArrayHasKey(2, $choices);

        $this->assertEquals('', $choices['']);
        $this->assertEquals('1', $choices[1]);
        $this->assertEquals('2', $choices[2]);
    }

    /**
     * @test
     * @throws ReflectionException
     */
    public function it_properly_converts_to_array()
    {
        $array = NullableEnum::asArray();

        $this->assertCount(3, $array);

        $this->assertArrayHasKey('None', $array);
        $this->assertArrayHasKey('Initialized', $array);
        $this->assertArrayHasKey('Completed', $array);

        $this->assertNull($array['None']);
        $this->assertEquals(1, $array['Initialized']);
        $this->assertEquals(2, $array['Completed']);

        // $this->assertInternalType('int', $array['Initialized']);
        // $this->assertInternalType('int', $array['Completed']);
    }
}

# vim: set ts=4 sw=4 tw=80 noet :
