<?php /** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * Bootstrap
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

/**
 * Bootstrap.
 *
 * @package Exen\Laravel\Enumeration\Tests
 */

use Exen\Laravel\Enumeration\PHPUnit\Comparator;
use SebastianBergmann\Comparator\Factory;

Factory::getInstance()->register(new Comparator());

# vim: set ts=4 sw=4 tw=80 noet :
