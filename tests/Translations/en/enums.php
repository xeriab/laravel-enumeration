<?php /** @noinspection PhpUnnecessaryCurlyVarSyntaxInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * enums
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

use Exen\Laravel\Enumeration\Tests\Enums\UserTypeLocalized;

return [
    UserTypeLocalized::class => [
        UserTypeLocalized::Administrator => 'Administrator',
        UserTypeLocalized::SuperAdministrator => 'Super administrator',
    ],
];

# vim: set ts=4 sw=4 tw=80 noet :
