<?php /** @noinspection PhpUnnecessaryCurlyVarSyntaxInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * EnumPipeValidationTest
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Tests;

use Exen\Laravel\Enumeration\Rules\EnumKey;
use Exen\Laravel\Enumeration\Tests\Enums\UserType;
use Illuminate\Support\Facades\Validator;

/**
 * EnumPipeValidationTest Class.
 *
 * @package Exen\Laravel\Enumeration\Tests
 */
class EnumPipeValidationTest extends ApplicationTestCase
{
    public function test_can_validate_value_using_pipe_validation()
    {
        $validator = Validator::make(['type' => UserType::Administrator], [
            'type' => 'enum_value:' . UserType::class,
        ]);

        $this->assertTrue($validator->passes());

        $validator = Validator::make(['type' => 99], [
            'type' => 'enum_value:' . UserType::class,
        ]);

        $this->assertFalse($validator->passes());
    }

    public function test_can_validate_value_using_pipe_validation_without_strict_type_checking()
    {
        $validator = Validator::make(['type' => (string) UserType::Administrator], [
            'type' => 'enum_value:' . UserType::class . ',false',
        ]);

        $this->assertTrue($validator->passes());
    }

    public function test_can_validate_key_using_pipe_validation()
    {
        $validator = Validator::make(['type' => UserType::getKey(UserType::Administrator)], [
            'type' => 'enum_key:' . UserType::class,
        ]);

        $this->assertTrue($validator->passes());

        $validator = Validator::make(['type' => 'wrong'], [
            'type' => 'enum_key:' . UserType::class,
        ]);

        $this->assertFalse($validator->passes());
    }

    public function test_can_validate_enum_using_pipe_validation()
    {
        $validator = Validator::make(['type' => UserType::Administrator()], [
            'type' => 'enum:' . UserType::class,
        ]);

        $this->assertTrue($validator->passes());

        $validator = Validator::make(['type' => 'wrong'], [
            'type' => 'enum:' . UserType::class,
        ]);

        $this->assertFalse($validator->passes());
    }
}

# vim: set ts=4 sw=4 tw=80 noet :
