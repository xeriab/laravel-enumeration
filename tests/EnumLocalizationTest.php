<?php /** @noinspection PhpUnnecessaryCurlyVarSyntaxInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * EnumLocalizationTest
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Tests;

use Exen\Laravel\Enumeration\Rules\Enum;
use Exen\Laravel\Enumeration\Rules\EnumKey;
use Exen\Laravel\Enumeration\Tests\Enums\UserType;
use Exen\Laravel\Enumeration\Tests\Enums\UserTypeLocalized;

/**
 * EnumLocalizationTest Class.
 *
 * @package Exen\Laravel\Enumeration\Tests
 */
class EnumLocalizationTest extends ApplicationTestCase
{
    public function test_enum_get_description_with_localization()
    {
        $this->app->setLocale('en');
        $this->assertSame('Super administrator', UserTypeLocalized::getDescription(UserTypeLocalized::SuperAdministrator));

        $this->app->setLocale('es');
        $this->assertSame('Súper administrador', UserTypeLocalized::getDescription(UserTypeLocalized::SuperAdministrator));
    }

    public function test_enum_get_description_for_missing_localization_key()
    {
        $this->app->setLocale('en');
        $this->assertSame('Moderator', UserTypeLocalized::getDescription(UserTypeLocalized::Moderator));

        $this->app->setLocale('es');
        $this->assertSame('Moderator', UserTypeLocalized::getDescription(UserTypeLocalized::Moderator));
    }

    public function test_can_localize_validation_error_message_using_class_rule()
    {
        $validator = $this->app['validator'];

        $this->assertSame(
            'The value you have provided is not a valid enum instance.',
            $validator->make(['input' => 'test'], ['input' => new Enum(UserType::class)])->errors()->first()
        );
        $this->assertSame(
            'Wrong key.',
            $validator->make(['input' => 'test'], ['input' => new EnumKey(UserType::class)])->errors()->first()
        );

        $this->app->setLocale('es');

        $this->assertSame(
            'The value you have provided is not a valid enum instance.', // No Spanish translations out of the box
            $validator->make(['input' => 'test'], ['input' => new Enum(UserType::class)])->errors()->first()
        );
        $this->assertSame(
            'Llave incorrecta.',
            $validator->make(['input' => 'test'], ['input' => new EnumKey(UserType::class)])->errors()->first()
        );
    }

    public function test_can_localize_validation_error_message_using_string_rule()
    {
        $validator = $this->app['validator'];

        $this->assertSame(
            'The value you have provided is not a valid enum instance.',
            $validator->make(['input' => 'test'], ['input' => 'enum:Exen\Laravel\Enumeration\Tests\Enums\UserType'])->errors()->first()
        );
        $this->assertSame(
            'Wrong key.',
            $validator->make(['input' => 'test'], ['input' => 'enum_key:Exen\Laravel\Enumeration\Tests\Enums\UserType'])->errors()->first()
        );

        $this->app->setLocale('es');

        $this->assertSame(
            'The value you have provided is not a valid enum instance.', // No Spanish translations out of the box
            $validator->make(['input' => 'test'], ['input' => 'enum:Exen\Laravel\Enumeration\Tests\Enums\UserType'])->errors()->first()
        );
        $this->assertSame(
            'Llave incorrecta.',
            $validator->make(['input' => 'test'], ['input' => 'enum_key:Exen\Laravel\Enumeration\Tests\Enums\UserType'])->errors()->first()
        );
    }
}

# vim: set ts=4 sw=4 tw=80 noet :
