<?php /** @noinspection PhpUnnecessaryCurlyVarSyntaxInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * VarExportTest
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Tests;

use PHPUnit\Framework\TestCase;
use Exen\Laravel\Enumeration\Tests\Enums\UserType;
use function var_export;

/**
 * VarExportTest Class.
 *
 * @package Exen\Laravel\Enumeration\Tests
 */
class VarExportTest extends TestCase
{
    public function test_var_export()
    {
        $admin = UserType::Administrator();

        $exported = var_export($admin, true);
        $restored = eval("return {$exported};");

        $this->assertSame($admin->value, $restored->value);
    }
}

# vim: set ts=4 sw=4 tw=80 noet :
