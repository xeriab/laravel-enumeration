<?php /** @noinspection PhpUnnecessaryCurlyVarSyntaxInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * EnumTypeTest
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Tests;

use Exen\Laravel\Enumeration\EnumType;
use Exen\Laravel\Enumeration\Tests\Enums\StringValues;
use Doctrine\DBAL\Platforms\MySQL57Platform;
use Doctrine\DBAL\Types\Type;

/**
 * EnumTypeTest Class.
 *
 * @package Exen\Laravel\Enumeration\Tests
 */
class EnumTypeTest extends ApplicationTestCase
{
    public function testIsRegistered(): void
    {
        $this->assertInstanceOf(
            EnumType::class,
            Type::getType(EnumType::ENUM)
        );
    }

    public function testGetSQLDeclaration(): void
    {
        $enumType = Type::getType(EnumType::ENUM);

        $this->assertSame(
            "ENUM('administrator','moderator')",
            $enumType->getSQLDeclaration(
                ['allowed' => StringValues::getValues()],
                new MySQL57Platform()
            )
        );
    }
}

# vim: set ts=4 sw=4 tw=80 noet :
