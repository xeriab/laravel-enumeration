<?php /** @noinspection PhpUnnecessaryCurlyVarSyntaxInspection */
/** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * EnumAttributeDescriptionTest
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

namespace Exen\Laravel\Enumeration\Tests;

use Exception;
use PHPUnit\Framework\TestCase;
use Exen\Laravel\Enumeration\Tests\Enums\DescriptionFromAttribute;

/**
 * EnumAttributeDescriptionTest Class.
 *
 * @package Exen\Laravel\Enumeration\Tests
 */
class EnumAttributeDescriptionTest extends TestCase
{
    public function test_enum_can_get_description_defined_using_attribute()
    {
        $this->assertSame('Admin', DescriptionFromAttribute::getDescription(DescriptionFromAttribute::Administrator));
        $this->assertSame('Mod (Level 1)', DescriptionFromAttribute::getDescription(DescriptionFromAttribute::Moderator));
    }

    public function test_enum_description_falls_back_to_get_description_method_when_not_defined_using_attribute()
    {
        $this->assertSame('Super Admin', DescriptionFromAttribute::getDescription(DescriptionFromAttribute::SuperAdministrator));
    }

    public function test_an_exception_is_thrown_when_accessing_a_description_which_is_annotated_with_multiple_description_attributes()
    {
        $this->expectException(Exception::class);

        DescriptionFromAttribute::InvalidCaseWithMultipleDescriptions()->description;
    }

    public function test_an_exception_is_not_thrown_when_accessing_a_description_for_an_invalid_value()
    {
        $this->assertSame('', DescriptionFromAttribute::getDescription('invalid'));
    }
}

# vim: set ts=4 sw=4 tw=80 noet :
