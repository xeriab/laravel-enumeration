<?php /** @noinspection PhpUnused */
/**
 * -*- tab-width: 4; encoding: utf-8; mode: php; -*-
 *
 * messages
 *
 * @copyright Copyright (c) 2019-2022 Xeriab Nabil. All rights reserved.
 * @author    hey@xeriab.com
 *
 * SPDX-License-Identifier: MIT
 */

declare(strict_types=1);

return [
    'enum'       => 'The value you have provided is not a valid enum instance.',
    'enum_value' => 'The value you have entered is invalid.',
    'enum_key'   => 'The key you have entered is invalid.',
];

# vim: set ts=4 sw=4 tw=80 noet :
