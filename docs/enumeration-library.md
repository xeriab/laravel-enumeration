# Enumeration Library

A list of commonly used, community contributed enumerations.

If you wish to contribute an enumeration, please submit via a pull request.

- [Test](#) - submitted by [Xeriab Nabil](https://gitlab.com/xeriab)
